**CGATES TV MOBILIOSIOS PROGRAMĖLĖS PRIVATUMO POLITIKA**
Ver. 2, galioja nuo 2024-01-30

**Bendra informacija**

Šioje Cgates TV mobiliosios programėlės („App“) privatumo politikoje („Politika“) aprašome App naudotojų (priklausomai nuo konteksto, įvardijama kaip „Jūs“, „Jūsų“, „Jums“ ar pan.) asmens duomenų („Asmens duomenys“) rinkimo, naudojimo ir apsaugos principus. Norėdami gauti detalesnę informaciją, kreipkitės Politikoje nurodytais kontaktais.

UAB Cgates (priklausomai nuo konteksto, įvardijama kaip „Duomenų valdytojas“, „Cgates“ „Mes“, „Mūsų“, „Mums“, „Mus“ ar pan.) yra Jūsų Asmens duomenų, tvarkomų, kai naudojatės App, valdytojas.

Duomenų valdytojo rekvizitai:
UAB Cgates
Juridinio asmens kodas: 120622256
Buveinės adresas: Ukmergės g. 120, LT-08105 Vilnius
Duomenų apsaugos pareigūno el. paštas: duomenu.apsauga@cgates.lt

Politiką taikome: kai naudojatės App; kai Asmens duomenis pradėjome tvarkyti iki Politikos įsigaliojimo. Jūsų Asmens duomenis tvarkome vadovaudamiesi Bendruoju duomenų apsaugos reglamentu Nr. 2016/679 („BDAR“), Asmens duomenų teisinės apsaugos įstatymu, Elektroninių ryšių įstatymu, kitais taikytinais teisės aktais ir Politika.

Naudodami App bei per App teikdami Mums Asmens duomenis, patvirtinate, kad jie yra teisingi ir tikslūs bei kad susipažinote su ir įsipareigojate laikytis Politikos.

Politikoje vartojamos sąvokos suprantamos taip, kaip jos yra apibrėžtos BDAR, nebent Politikoje nurodyta kitaip.

Jūs galite naudotis App 1) sudarę sutartį su Mumis, kaip klientas, ar 2) būdami abonentu, kai sutartį su Cgates sudarė kitas asmuo. Ribota apimtimi (galima matyti, koks turinys prieinamas App, be teisės jį peržiūrėti) App galite naudoti apsilankę App, kaip vartotojas, nesudaręs su Cgates sutarties. Visais šiais atvejais vadovaujamasi šia Politika. Jei leisite kitam vartotojui naudotis App pagal sutartį tarp Jūsų ir Cgates, taip pat turite užtikrinti, kad naudotojas būtų susipažinęs su Politika.

Jaunesniems negu 14 metų asmenims draudžiama App pateikti savo Asmens duomenis, jei prieš pateikiant juos negautas tėvų arba kitų teisėtų atstovų sutikimas ir sutikimas iš anksto nepateiktas Mums. Vyresni nei 14 metų, bet jaunesni negu 18 metų asmenys, prieš pateikdami App savo Asmens duomenis, privalo turėti tėvų arba kitų teisėtų atstovų sutikimą ir Mums prašant privalo nedelsiant jį pateikti. Jei pateikiate ne savo Asmens duomenis, privalote turėti asmens, kurio Asmens duomenis teikiate, sutikimą ir Mums prašant privalote nedelsiant pateikti tokį sutikimą.

Cgates gali iš anksto Jums nepranešusi keisti Politiką. Cgates atlikus esminius Politikos pakeitimus, apie tai informuosime Jus.


**Techniniai reikalavimai įrenginiui naudoti App**

App galima naudotis šiuose įrenginiuose:

1.	„Android“ ir „iOS“ išmaniuosiuose telefonuose ir planšetėse - atsisiuntus „Cgates TV“ programėlę;
2.	„Amazon Fire TV“ įrenginyje - atsisiuntus Cgates TV programėlę;
3.	Kompiuteriuose - naudojant interneto naršyklę, suvedus nuorodą https://cgates.tv;
4.	„Android TV“ operacinę sistemą turinčiuose televizoriuose - atsisiuntus Cgates TV programėlę;
5.	„Android TV“ operacinės sistemos neturinčiuose televizoriuose - naudojant CGATES TV Android multimedijos įrenginį.

App parsisiųsti galima iš:

1.	Huawei App Gallery;
2.	Google Play Store;
3.	Amazon Store;
4.	Apple App Store;
5.	Samsung App Store;
6.	LG App Store.

App tinkamam veikimui įrenginyje turi būti įdiegta ši operacinės sistemos versija:

1.	ANDROID TV1 APP: Android TV 7.0+;
2.	AMAZON FIRE TV Stick: FireOS 6+;
3.	ANDROID telefonas: Android 7.0+;
4.	ANDROID planšetė: Android 7.0+;
5.	APPLE telefonas (iPhone): iOS 13+;
6.	APPLE planšetė (iPad): iOS 13+;
7.	PC CHROME naršyklė: Chrome 81+ (paskutinės 2 naujausios versijos);
8.	PC MICROSOFT naršyklė: Edge 40+ (paskutinės 2 naujausios versijos);
9.	PC FIREFOX naršyklė: Firefox 81+ (paskutinės 2 naujausios versijos);
10.	PC OPERA naršyklė: Opera 52+ (paskutinės 2 naujausios versijos).

Televizorių gamintojai, kurie turi modelius su „Android TV“ operacine sistema:

1.	Xiaomi TV („Android“ v7 ir naujesni);
2.	Nokia TV („Android“ v7 ir naujesni);
3.	Sony TV („Android“ v7 ir naujesni);
4.	Philips TV („Android“ v7 ir naujesni);
5.	Hisense TV („Android“ v7 ir naujesni);
6.	Siemens TV („Android“ v7 ir naujesni).

**Sąvokos**

**Klientas** – fizinis asmuo, kuris yra pasirašęs Cgates paslaugų teikimo sutartį ir naudojasi Cgates teikiamomis elektroninių ryšių paslaugomis asmeniniams, šeimos ar namų ūkio poreikiams, nesusijusiems su verslu ar profesija, tenkinti.

**Asmens duomenys** – bet kuri informacija, susijusi su fiziniu asmeniu – duomenų subjektu, kurio tapatybė yra žinoma arba gali būti tiesiogiai ar netiesiogiai nustatyta pasinaudojant atitinkamais duomenimis.

**Paslaugos** – bet kokie tiek elektroniniu, tiek neelektroniniu būdu Cgates siūlomi produktai ir (ar) paslaugos.

**Kokius Jūsų duomenis, kokiu tikslu ir pagrindu tvarkome, kiek laiko saugome?**

Duomenų tvarkymo tikslas	Tvarkomi asmens duomenys	Teisinis pagrindas	Saugojimo terminas
App paskyros sukūrimas ir naudojimas	El. pašto adresas, telefono Nr., vardas, pavardė, miestas, slaptažodis, nustatytos paskyros profilio pasirinktys	Sutarties vykdymas	Asmens duomenys, jeigu klientas visiškai atsiskaitė už Paslaugas, saugomi 10 metų nuo Paslaugų sutarties nutraukimo kalendorinių metų pabaigos.
Asmens duomenys, kai klientas nevisiškai atsiskaitė už Paslaugas - saugomi 10 metų nuo įsiskolinimo panaikinimo kalendorinių metų pabaigos.
Paslaugų sutarties vykdymas ir buhalterinė apskaita	Turinio naudojimo (filmų, kanalų, laidų, įrašų peržiūros faktas, žiūrimumo statistika, individualizuotos rekomendacijos turinio peržiūrai, data ir laikas, trukmė) duomenys, įrenginio IP adresas, informacija apie peržiūros įrenginio tipą, kliento identifikacinis numeris, per App užsakytos mokamos paslaugos ir jų kaina, buhalterinės apskaitos dokumentuose užfiksuoti turinio naudojimo duomenys, paslaugų buhalterinės apskaitos ir paslaugų apmokėjimo dokumentuose užfiksuoti duomenys	Sutarties vykdymas
	Asmens duomenys, jeigu klientas visiškai atsiskaitė už Paslaugas, saugomi 10 metų nuo Paslaugų sutarties nutraukimo kalendorinių metų pabaigos.
Asmens duomenys, kai klientas nevisiškai atsiskaitė už Paslaugas - saugomi 10 metų nuo įsiskolinimo panaikinimo kalendorinių metų pabaigos.
Techninio aptarnavimo užklausų administravimas	App techninių trikdžių šalinimo ir aptarnavimo metu užfiksuoti duomenys (užklausa, telefono pokalbio įrašymas, priežiūros atlikimo metu užfiksuoti duomenys), el. pašto adresas, tel. Nr., turinio naudojimo duomenys	Sutarties vykdymas
Teisėtas Cgates interesas fiksuoti tinkamo sutarties vykdymo įrodymus	Asmens duomenys, jeigu klientas visiškai atsiskaitė už Paslaugas, saugomi 10 metų nuo Paslaugų sutarties nutraukimo kalendorinių metų pabaigos.
Asmens duomenys, kai klientas nevisiškai atsiskaitė už Paslaugas - saugomi 10 metų nuo įsiskolinimo panaikinimo kalendorinių metų pabaigos.
Skundų administravimas	Skunde ar užklausoje nurodyta informacija	Teisinė prievolė	Kol bus galutinai išspręstas skundas ir 10 metų po kalendorinių metų, kuriais galutinai išspręstas skundas, pabaigos.
Skolų administravimas	Skolos, jos administravimo ir išieškojimo duomenys	Teisėtas Duomenų valdytojo interesas išieškoti skolą už Paslaugas	10 metų nuo įsiskolinimo panaikinimo kalendorinių metų pabaigos.
App techninio veikimo užtikrinimas	Slapukų pagalba surinkta App naudojimo informacija.
Daugiau - Politikos skyriuje „Slapukai“	Teisėtas duomenų valdytojo interesas užtikrinti App tinkamą veikimą arba sutikimas	Politikos skyriuje „Slapukai“
Cgates teisių gynimas valstybinėse institucijose ar teisme (esant poreikiui)	Cgates turimi Asmens duomenys, kurių gali prireikti Cgates teisių gynimui	Teisėtas Duomenų valdytojo interesas įrodyti tinkamą Paslaugų suteikimą ar apginti teises	Iki galutinio ir neskundžiamos sprendimo priėmimo ir 10 metų po kalendorinių metų, kuriais galutinai išspręstas pareikštas reikalavimas, pabaigos.
Asmeninių pasiūlymų pateikimas (profiliavimas rinkodaros tikslu)	Duomenys apie App paslaugų naudojimą, el. pašto adresas, tel. numeris	Sutikimas	Iki kol atšauksite duotą sutikimą
Verslo analitika	Duomenys apie App paslaugų naudojimą	Teisėtas Duomenų valdytojo interesas tobulinti produktus ir paslaugas, plėsti veiklą	Verslo plėtros veiklos planavimo ir įgyvendinimo metu


**Ar būtina pateikti Asmens duomenis?**

Asmens duomenis būtina pateikti App paskyros sukūrimo ir naudojimo, Paslaugų sutarties vykdymo ir buhalterinės apskaitos tikslais, jei naudojatės mokamomis Cgates TV paslaugomis ir esate sudaręs sutartį su Cgates, kad galėtumėte naudotis pilnu App funkcionalumu. Kitais Asmens duomenų tvarkymo tikslais savo Asmens duomenų pateikti nėra būtina, tačiau tokiu atveju negalėsite naudotis dalimi App Paslaugų.

**Informacija iš trečiųjų šalių**

Cgates gali papildyti informaciją, kuri gaunama iš Kliento, ir informaciją apie Kliento naudojimąsi Cgates paslaugomis viešai prieinama informacija apie Klientą, taip pat informacija, kurią Cgates gauna iš trečiųjų asmenų arba per slapukus ar panašias priemones. Slapukų generuojama informaciją apie App Paslaugų naudojimą Cgates gali susieti su kitais Cgates turimais Jūsų Asmens duomenimis, naudojamas verslo analitikos ir profiliavimo tikslais per Jūsų el. p. adresą ir Kliento identifikacinį numerį.

**Ką darome, kad apsaugotume Jūsų informaciją?**

Apsaugoti Asmens duomenis surinktus naudojantis App, Cgates įgyvendino atitinkamas organizacines ir technines saugumą užtikrinančias priemones, kurios padeda apsaugoti Asmens duomenis nuo atsitiktinio ar neteisėto sunaikinimo, pakeitimo, atskleidimo, taip pat nuo bet kokio kito neteisėto tvarkymo.

Klientas atsako už App paskyros prisijungimo slaptažodžio neatskleidimą. Esant galimybei, po kiekvieno prisijungimo, Klientui reikėtų atsijungti nuo App paskyros visų įrenginiuose, kurie yra vieši arba kuriais dalinamasi. Jeigu Klientas grąžina arba parduoda įrenginį, kurio pagalba galėjo naudotis Cgates Paslaugomis, pirmiausia, reikia atsijungti nuo Paskyros įrenginyje. Jeigu Kliento slaptažodis arba įrenginys yra nesaugūs, arba Klientas neatsijungė nuo Paskyros įrenginyje, paskesni naudotojai gali gauti prieigą prie Kliento paskyros ir asmeninės informacijos.

Informacijos perdavimas internetu nėra iki galo saugus. Nepaisant visų Cgates pastangų, negalime garantuoti Kliento informacijos absoliutaus saugumo ir prisiimti atsakomybės dėl galimos neteisėtos prieigos prie šios informacijos. Klientas savo rizika pateikia informaciją App. Atsižvelgiant į tai, kad Cgates turi prieigą prie Kliento informacijos, naudojame saugos priemones, stengiantis užkirsti kelią neteisėtai prieigai prie Kliento informacijos.


**Iš ko gauname Jūsų Asmens duomenis?**

Jūsų Asmens duomenis gauname tiesiogiai iš Jūsų, sugeneruojame patys Jums naudojantis App ir/ar galime juos gauti iš trečiųjų asmenų, tokių kaip:
•	Mokėjimo paslaugas teikiančių partnerių (bankai, mokėjimo įstaigos);
•	Kompetentingų valstybės ar teisėsaugos institucijų, įpareigojančių Mus atlikti tam tikrus veiksmus Jūsų ar Jūsų Asmens duomenų atžvilgiu.

**Kam atskleidžiame Jūsų Asmens duomenis?**

Mes atskleisime Jūsų Asmens duomenis:
•	Kai Mes teikiame Paslaugas bendradarbiaudami su partneriais – Mūsų partneriams, veikiantiems kaip duomenų tvarkytojai (pvz., turinį teikiantys partneriai gauna tik kliento identifikacinį numerį be jokios kitos klientą tiesiogiai identifikuoti galinčios informacijos; duomenų prieglobos, IT sistemų palaikymo paslaugų teikėjai; aptarnavimo telefonu agentūroms) arba savarankiškiems duomenų gavėjams (partneriams, teikiantiems apmokėjimo administravimo ar kitas Jūsų užsakytomis Paslaugomis susijusias paslaugas; kitiems reikalingas paslaugas teikiantiems asmenims, tokiems kaip teisininkai, auditoriai, konsultantai ir t.t.). Šių gavėjų galimybė naudoti Jūsų Asmens duomenis yra ribota. Jie negali šios informacijos naudoti kitais nei paslaugų teikimo Mums tikslais. Šiems paslaugų teikėjams mes suteiksime tik tiek Jūsų Asmens duomenų, kiek bus būtina konkrečiai paslaugai suteikti.
•	valstybės ar teisėsaugos institucijoms ar kitiems subjektams, vadovaujantis taikytinais teisės aktais, kai to reikalaujama pagal įstatymus arba tai yra

**Asmeninių pasiūlymų pateikimas (profiliavimas rinkodaros tikslu)**

Tiesioginės rinkodaros tikslu vykdome profiliavimą (t.y., automatizuotą asmens duomenų tvarkymą, kai asmens duomenys naudojami siekiant įvertinti tam tikrus su fiziniu asmeniu susijusius asmeninius aspektus ir priskirti App naudotojus tam tikroms kategorijoms). Profiliavimas Jums nesukelia teisinių pasekmių ar panašaus reikšmingo poveikio. Tvarkome Jūsų duomenis apie App paslaugų naudojimą, siekdami pateikti asmeniškai Jums pritaikytus pranešimus (el. paštu, skambučiu, trumpąja SMS žinute). Profiliavimas vykdomas tik Jūsų sutikimo pagrindu (duodamas sutinkant su tiesiogine rinkodara), o asmeniškai Jums pritaikyta rinkodara siunčiama Jums Jūsų sutikimo pagrindu arba Mūsų teisėto intereso skatinti prekybą ir teikti Jums aktualius pasiūlymus pagrindu (kai esate Mūsų klientas ir neprieštaravote tiesioginės rinkodaros gavimui).

**Jūsų teisės**

Jūs turite šias teises:
•	Žinoti apie savo Asmens duomenų tvarkymą (teisė žinoti);
•	Susipažinti su savo Asmens duomenimis ir kaip jie tvarkomi (teisė susipažinti);
•	Reikalauti ištaisyti arba papildyti neišsamius Asmens duomenis (teisė ištaisyti);
•	Reikalauti savo Asmens duomenis ištrinti arba sustabdyti savo Asmens duomenų tvarkymo veiksmus (išskyrus saugojimą) (teisė sustabdyti duomenų tvarkymą ir teisė „būti pamirštam“);
•	Reikalauti, kad Duomenų valdytojas apribotų Asmens duomenų tvarkymą esant teisėtai priežasčiai (teisė apriboti);
•	Gauti Asmens duomenis, kuriuos pateikėte Duomenų valdytojui susistemintu, įprastai naudojamu ir kompiuterio skaitomu formatu, ir persiųsti tuos Asmens duomenis kitam duomenų valdytojui, esant teisėtai priežasčiai (teisė į perkeliamumą);
•	Nesutikti, kad būtų tvarkomi Asmens duomenys, kai šie Asmens duomenys tvarkomi sutikimo ar teisėto intereso pagrindu (išskyrus kai Duomenų valdytojo interesas tvarkyti Jūsų Asmens duomenis viršesnis nei Jūsų interesas), įskaitant teisę nesutikti, kad Asmens duomenys būtų tvarkomi tiesioginės rinkodaros tikslu, įskaitant profiliavimą (teisė nesutikti);
•	Bet kuriuo metu atšaukti savo duotą sutikimą tvarkyti Jūsų Asmens duomenis sutikime nurodytais tikslais, pavyzdžiui, vykdyti tiesioginę rinkodarą, įskaitant profiliavimą. Toks sutikimo atšaukimas neturės įtakos iki sutikimo atšaukimo vykdyto Asmens duomenų tvarkymo teisėtumui (teisė atšaukti sutikimą);
•	Manydami, kad pažeidžiame Jūsų teises, teisės aktų nustatyta tvarka galite pateikti skundą Valstybinei duomenų apsaugos inspekcijai.

Siekdama(s) įgyvendinti savo, kaip duomenų subjekto, teises, turite užpildyti laisvos formos prašymą dėl duomenų subjekto teisių įgyvendinimo. Jį galite 1) pasirašyti įprastai ir pateikti atvykę į Duomenų valdytojo biurą (adresas nurodytas prie Duomenų valdytojo rekvizitų); arba 2) galite pasirašyti užpildytą prašymą kvalifikuotu el. parašu ir atsiųsti Cgates duomenų apsaugos pareigūnui el. paštu (adresas nurodytas prie Duomenų valdytojo rekvizitų).

Atvykus prašymą pateikti į biurą, prašysime Jūsų pateikti originalų pasirašytą prašymą bei savo asmens tapatybės dokumentą tapatybei patikrinti. Taip įsitikiname Jūsų tapatybe ir galime išvengti neleistino Jūsų Asmens duomenų tvarkymo. Tik nurodytu būdu pateiktus prašymus laikome pateiktais, juos vertiname bei įgyvendiname. 

Gavę tokį prašymą, ne vėliau kaip per vieną mėnesį nuo kreipimosi dienos, pateiksime atsakymą. Prireikus nurodytas laikotarpis gali būti pratęstas dar dviem mėnesiams, atsižvelgiant į prašymų sudėtingumą ir skaičių. Tokiu atveju, per vieną mėnesį nuo prašymo gavimo dienos informuosime apie tokį pratęsimą, kartu pateikdami vėlavimo priežastis. Teisės aktų numatytą terminą duomenų subjekto teisių įgyvendinimui pradedame skaičiuoti nuo tinkamo prašymo pateikimo vienu iš minėtų būdų.

**Slapukai**

Naudojame slapukus App veikimui užtikrinti bei kad galėtume suasmeninti jums rodomus skelbimus ir analizuoti naudojimąsi App.

App parinkčių skiltyje sutikdami su slapukų naudojimu, Jūs duodate sutikimą Mums įdiegti analitinius ir rinkodaros bei pakartotinės reklamos slapukus į Jūsų įrenginį. Jei nesutiksite su slapukų naudojimu ir toliau naudosite App, tuomet tik būtini App veikimui (techniniai ir funkciniai) slapukai, kuriems įdiegti neprivalome gauti Jūsų sutikimo, bus įdiegti į Jūsų įrenginį. Sutikdami su slapukais, Jūs taip pat sutinkate, kad tretieji asmenys gali įdiegti nurodytus slapukus ir naudoti per Juos gautą informaciją.

Slapukas yra raidžių ir skaitmenų laikmena, kurią išsaugome Jūsų įrenginyje. App veikimui užtikrinti naudojame būtinuosius (techniniai ir funkciniai) slapukus, kuriems įdiegti į Jūsų įrenginį neprivalome gauti Jūsų sutikimo.

App naudojami šių tipų slapukai:
•	Techniniai slapukai. Techniniai slapukai yra būtini tinkamam mūsų svetainės funkcionavimui. Techniniai slapukai padeda atvaizduoti Jums Mūsų svetainės turinį Jūsų įrenginyje. Be jų negalima sudaryti galimybės naudotis Mūsų svetaine visa apimtimi. Techniniai slapukai taip pat užtikrina, kad Jums davus sutikimą to nereikės daryti pakartotinai kiekvieną kartą Jums besilankant Mūsų interneto svetainėje.
•	Funkciniai slapukai. Funkciniai slapukai yra skirti pagerinti interneto svetainės funkcionalumą, padaryti interneto svetainę patogią bei efektyvią Jūsų naudojimui.
•	Analitiniai slapukai. Analitiniai slapukai, kurie naudojami Mūsų svetainės lankytojų navigacijos metodų statistinei analizei parengti; šių slapukų surinkti duomenys naudojami Jūsų neidentifikuojant, t.y. gauname nuasmenintą statistinę informaciją;
•	Rinkodaros bei pakartotinės reklamos slapukai. Tai slapukai, renkantys Jūsų apsilankymo Mūsų svetainėje informaciją, kuri vėliau naudojama pakartotinei reklamai demonstruoti internete (įskaitant ir įvairius socialinius tinklus („Facebook“ ir pan.), vykdyti elektroninės rinkodaros kampanijas, taip pat išmatuojant Mūsų rinkodaros veiksmų sėkmingumą. Dauguma šių slapukų yra trečiųjų šalių slapukai (apie juos skaitykite žemiau), kurias mes pasitelkiame rinkodaros/pakartotinės reklamos skleidimo tikslais.
•	Sesijos slapukai – slapukai, naudojami, kol lankytojas naršo internete ir ištrinami automatiškai uždarius naršyklę;
•	Pastovūs slapukai saugomi lankytojo naršyklėje, kol sueina slapuko galiojimo laikas arba kol lankytojas pats juos ištrina.
•	Pirmosios šalies slapukai: tai yra Mūsų pačių slapukai. 
•	Trečiosios šalies slapukai: šiuos slapukus pagal susitarimą su Mumis pateikia trečiosios šalys.


**Slapukų naudojimas analitikai, rinkodarai bei pakartotinei reklamai**

Google Analytics. Naudojame „Google Analytics“ App lankytojų naudojimo analizavimo paslaugą teikiamą „Google“. „Google Analytics“ – tai „Google“ analizės įrankis, kurį naudodami svetainių ir programėlių savininkai gali lengviau suprasti, kaip lankytojai naudoja jų svetainės arba programėlės paslaugas. Naudojame slapukų rinkinį informacijai rinkti ir ataskaitai apie App naudojimo statistiką sistemoje „Google“ pateikti, asmeniškai neidentifikuojant atskirų lankytojų.

Jei prieštaraujate slapukų įrašymui į Jūsų įrenginį analizės tikslais, Jūs galite pasinaudoti „Google“ suteikiama galimybe įsidiegti dažniausiai naudojamų interneto naršyklių plėtinį, kuris išjungia „Google Analytics“ veikimą Jums naršant internete. Plėtinį galite atsisiųsti adresu https://tools.google.com/dlpage/gaoptout?hl=en. Įdiegus šį plėtinį, „Google Analytics“ veikimas bus išjungtas visiškai, ne tik Mūsų App.

Taip pat informuojame, kad App naudojame papildomas „Google Analytics Advertising features“ funkcijas „Demografiniai rodikliai ir pomėgiai“ bei „Pakartotinė reklama“. „Demografiniai rodikliai ir pomėgiai“ funkcijos veikimui mes naudojame „Google Analytics“ slapukus, kad galėtumėme gauti agreguotą ir anoniminę demografinę bei pomėgių informaciją apie App lankytojus. „Pakartotinė reklama“ šios funkcijos pagalba App lankytojams ar susidomėjusiems Mūsų paslaugomis per reklaminį pranešimą, pakartotinai teikti Mūsų administruojamus reklaminius pranešimus „Google“ partnerių svetainėse. Jūs turite galimybę išjungti personalizuotą pakartotinės reklamos rodymą pakeisdami nustatymus https://adssettings.google.com.

Daugiau informacijos, kaip „Google“ naudoja Jūsų informaciją reklamos skelbimui https://safety.google/privacy/ads-and-data/?hl=lt.

Facebook Pixel. App yra naudojama Meta Platforms Inc. (“Facebook”) teikiama pakartotinės reklamos funkcija. Ši funkcija yra naudojama siekiant optimizuoti reklamą, kuri yra rodoma App lankytojams, kai jie naudojasi „Facebook“ socialiniu tinklu ar kitomis svetainėmis, kuriose įdiegta Facebook Pixel technologija. Šiam tikslui pasiekti App įdiegta Facebook pakartotinės reklamos žyma (FB pikselis). Jums naudojantis App, žyma išsaugo informaciją apie tai. Kai prisijungiate prie savo paskyros „Facebook“ informacija apie Jūsų naudojimąsi App yra perduodama Facebook. Šios funkcijos veikimo metu renkama informacija yra anoniminė, t.y. Mes negalime Jūsų identifikuoti, remiantis šios funkcijos veikimo palaikymui surinkta informacija. Daugiau informacijos apie Facebook teikiamas pakartotinės reklamos paslaugas galite rasti „Facebook“ privatumo politikoje https://www.facebook.com/about/privacy/. Jūs taip pat turite galimybę išjungti „Facebook“ atliekamą pakartotinę reklamą, tai galite padaryti apsilankę https://www.facebook.com/settings (tam, kad galėtumėte išjungti personalizuotą „Facebook“ reklamą, turite prisijungti prie savo „Facebook“ paskyros).


**Slapukų naudojimo ribojimas bei atsisakymas**

Slapukus kontroliuojate Jūs, todėl galite bet kuriuo metu jų atsisakyti arba juos sunaikinti (ištrinti, išdiegti), koreguodami savo App ir įrenginio/interneto naršyklės nustatymus. Įprastai Jūs galite:
•	patikrinti, kokie slapukai yra įrašyti, ir ištrinti atskirus slapukus;
•	blokuoti trečiųjų šalių slapukus;
•	blokuoti slapukus iš konkrečių interneto svetainių;
•	blokuoti visų slapukų įrašymą;
•	uždarant naršyklę ištrinti visus slapukus.

Jei nesutinkate, kad į Jūsų įrenginį būtų įrašomi slapukai, galite bet kada atšaukti sutikimą juos naudoti, pakeitę nustatymus ir ištrynę įrašytus slapukus taip pat aukščiau paminėtais būdais išjungti „Facebook“ ir/arba „Google“ atliekamą pakartotinės reklamos teikimą bei analitikos duomenų rinkimą. Jei pasirinkote ištrinti slapukus, nepamirškite, kad bus pašalintos ir visos nustatytos parinktys. Primename, jog visiškai užblokavus slapukus, daug interneto svetainių neveiks tinkamai.

Daugiau informacijos apie slapukus galite rasti: www.AllAboutCookies.org bei http://www.youronlinechoices.eu.

Informaciją, kaip pakeisti Jūsų naršyklės nustatymus, rasite:
Internet Explorer / Firefox / Google Chrome / Safari / Opera


**Informacija apie Mūsų naudojamus slapukus:**

Slapukas	Šaltinis	Tipas	Tikslas	Sukūrimo momentas / saugojimo trukmė

_app_lang	cgates.tv	Funkcinis, pirmosios šalies, pastovus	App profilio parinktos kalbos išsaugojimui	Apsilankius tinklapyje / 1 metai
connect.sid	cgates.tv	Funkcinis, pirmosios šalies, pastovus	Saugo prisijungimo informaciją naudotojo autentifikacijai ir saugiam prisijungimui	Apsilankius tinklapyje / 1 valanda
fusionauth.locale	cgates.tv	Funkcinis, pirmosios šalies, pastovus	Saugoti individualius pasirinkimus.	Apsilankius tinklapyje / 13 mėnesių
fusionauth.remember-device	cgates.tv	Funkcinis, pirmosios šalies, pastovus	Saugo informaciją, kad naudotojas pasirinko prisiminti prisijungimo prie App paskyros duomenis įrenginyje
	Apsilankius tinklapyje / 13 mėnesių
fussionauth.sso	cgates.tv	Funkcinis, pirmosios šalies, pastovus	Saugo informaciją, kad naudotojas pasirinko neprisiminti prisijungimo prie App paskyros duomenų įrenginyje
	Apsilankius tinklapyje / 13 mėnesių
fussionauth.timezone	cgates.tv	Funkcinis, pirmosios šalies, sesijos	Saugoma įrenginyje nustatyta laiko juosta tikslinti App atvaizduojamą laiką ir datą	Apsilankius tinklapyje / iki sesijos pabaigos (kol naudojamas App)
profile	cgates.tv	Funkcinis, pirmosios šalies, pastovus	Saugo nustatytas App profilio pasirinktis	Apsilankius tinklapyje / 1 valanda
_ga_R6G6PKD419	cgates.tv	Analitinis, pirmosios šalies, pastovus	Puslapio peržiūrų skaičiaus fiksavimas	Apsilankius tinklapyje / 2 metai
_ga	cgates.tv	Analitinis, pirmosios šalies, pastovus	Puslapio peržiūrų skaičiaus fiksavimas	Apsilankius tinklapyje / 2 metai
_ga	cgates.lt	Analitinis, pirmosios šalies, pastovus	Puslapio peržiūrų skaičiaus fiksavimas	Apsilankius tinklapyje / 2 metai
_ga_WQSDLCXLOP	cgates.lt	Analitinis, pirmosios šalies, pastovus	Puslapio peržiūrų skaičiaus fiksavimas	Apsilankius tinklapyje / 2 metai
_fbp	Meta Platforms Inc.	Rinkodaros bei pakartotinės reklamos, trečiosios šalies, pastovus	Fiksuoti App lankytojo lankymąsi kitose nei App svetainėse ir pateikti jose reklamą	Apsilankius tinklapyje / 3 mėnesiai
_gid	cgates.tv	Analitinis, pirmosios šalies, pastovus	Puslapio peržiūrų skaičiaus fiksavimas	Apsilankius tinklapyje / 24 valandos
_gcl_au	cgates.lt	Rinkodaros bei pakartotinės reklamos, pirmosios šalies, pastovus	Fiksuoti konversijoms	Apsilankius tinklapyje / 90 dienų

![](RackMultipart20210603-4-10h6pm3_html_f6274a618d2a53d.jpg)