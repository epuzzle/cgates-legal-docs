**CGATES COOKIES POLICY**

Cgates protects the privacy of its customers, so in this notification we clearly and openly present the principles of collecting and using information about visitors on our website, as well as the data protection applied by Cgates.

We apply this Privacy (Cookies) Policy (hereinafter referred to as the Policy) and data protection when you visit Cgates website, register in Cgates self-service platform, use Cgates TV smart TV and/or app or order goods and/or services in Cgates e-store and when you use Cgates services. This Policy does not apply when you browse the websites of other companies or use third party services accessing them through Cgates network.

We collect data of Cgates website visitors and process Cgates customers’ data in accordance with provisions of Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (the General Data Protection Regulation) (hereinafter referred to as the GDPR) and the Law of the Republic of Lithuania on the Legal Protection of Personal Data and the Law of the Republic of Lithuania on Electronic Communications and other legal acts on data protection.

You can use Cgates services by entering into an agreement as a customer, by being a subscriber when the agreement is concluded by another person, or by simply visiting our websites or programs as a user without an agreement with Cgates. The Policy applies in all these cases. If you allow another user to use the services under the agreement between you and UAB Cgates, you must also ensure that the user knows this Policy.

**Definitions**

**Customer** shall mean a natural person, who has signed a Cgates services agreement and uses Cgates electronic communications services for personal, family or household needs that are not related to business or profession.

**Personal data** shall mean any information relating to a natural person – data subject, whose identity is known or can be directly or indirectly identified by use of relevant data.

**Services** shall mean any products and/or services offered by Cgates, both electronically and not.

**Traffic data** shall mean data processed for the purpose of transmitting information by electronic communications network and/or for accounting of such transmission. The use of electronic communications services generates data that can, for example, show information about subscribers and terminal equipment used by them, the start and end time, duration and route of connection, data transmission protocol, the amount of data transmitted, the geographical location of terminal equipment of the actual user of electronic communications services, the format of data transmitted over the communication network, and other data processed in electronic communications networks or in the provision of electronic communications services. If traffic data directly or indirectly allow identification of a person, they shall also be considered as personal data.

**Data collected**

When administering the website, diagnosing malfunctions in the operation of Cgates servers, IP addresses of visitors' computers can be used. IP address is a unique code identifying a computer in the networks. It can be used to identify a visitor and to collect various demographic information. This is a common working practice of administrators of web servers followed by administrators of most web servers.

Cookies help us to collect data about use of services. The types of cookies and their purpose are given in the table below.

When a user registers in Cgates self-service platform, main information required for user identification is collected, which the user provides when filling in the registration form, i.e. name, surname, e-mail address and telephone number, sometimes address. Cgates can capture the customer’s correspondence address if the customer contacts us and it is technically possible.

In case of buying goods or services in Cgates e-store or otherwise, data necessary for the proper execution of orders are collected, for example, a device and details of the order from it, contact details and associated records. Information on payments and other financial information, such as type of a bank card, part of its number, expiry date, registered address or PayPal or the paying agent account details, bank account number, payment information.

Records of telephone conversations when a customer calls Cgates customer service centre or the customer takes calls from the Company’s staff.

User information provided by the customer for survey purposes (ratings, feedback, replies to queries and other requests which the customer does not have to respond to).

Other data collected on the basis of the customer's consent and which are defined in detail at the time when the customer’s consent is requested.

Cgates can supplement information received from the customer and information about the customer's use of Cgates services with publicly available information about the customer, also information that Cgates obtains from third parties or by use of cookies or similar devices.

It is important for Cgates to protect your data collected while visiting Cgates website, therefore Cgates implemented relevant organisational and technical security measures to protect personal data from accidental or unlawful destruction, modification, disclosure, also from any other unlawful processing.

**Use of cookies**

In order to improve your experience when visiting Cgates website, we use cookies – small files of textual information, which are automatically created when browsing the website and are stored on your computer or other terminal equipment. Cookies also help to ensure the smooth functioning of the website, to monitor the duration, frequency of visits to Cgates website and to collect statistical information about the number of visitors to the website. By analysing this data, we can improve the website, make it more user-friendly. By visiting the Cgates website you can choose whether you want to use cookies. If you do not agree to cookies being stored on your computer or another device, you may cancel your consent to use them at any time by changing settings and deleting saved cookies. More information is available at  [AllAboutCookies.org](http://allaboutcookies.org/) or [www.google.com/privacy_ads.html](http://www.google.com/privacy_ads.html).

**Information on cookies used**

|**Technical cookies of the website**|
| :- |
|**Cookie name**|**Description**|**Moment of creation**|**Validity**|**Data used**|
|cp-impression-added-forcp\_id\_dff3d|The cookie is used to save information about pop-up windows viewed and hidden by the customer.|At the time of the first visit to a page|Until the website window is closed|Unique identifier|
|cookie\_notice\_accepted|The cookie is intended for realisation of the Privacy (Cookies) Policy notification/consent.|When the user accepts the policy provisions|1 year |Unique identifier|
|<p>devicePixelRatio</p><p>testing<br>time</p>|The cookie is intended for realisation of the functionality of the website.|At the time of the first visit to a page|Until the website window is closed|Unique identifier|
|x-csrf-jwt|The cookie is intended for realisation of speed meter “cgates.speedtestcustom.com” functionality.|At the time when the speed meter is activated|Until the website window is closed|Unique identifier|
|**Google Tag Manager, Google Analytics, Google DoubleClick, Facebook Pixel cookies:**|
|\_dc\_gtm\_UA-25484389-1|The cookie is used to launch Google Analytics script and collects information about user behaviour on the website and is used to optimize and present offers on other websites.|At the time of visiting a page|Until the website window is closed|Unique identifier|
|\_ga|The cookie collects information about user behaviour on the website and is used to save statistical data.|At the time of the first visit to a page|2 years|Unique identifier|
|\_gid|The cookie collects information about user behaviour on the website and is used to save statistical data.|At the time of visiting a page|Until the website window is closed|Unique identifier|
|<p>ads/ga-audiences</p><p>ads/user-lists/#</p>|It is a pixel cookie that collects information about user behaviour on the website and is used to save statistical data.|At the time of visiting a page|Until the website window is closed|Unique identifier|
|collect|It is a pixel cookie that collects information about user behaviour on the website and is used to save statistical data, to optimise and present offers on other websites.|At the time of visiting a page|Until the website window is closed|Unique identifier|
|IDE|The cookie collects information about user behaviour in Google DoubleClick network and is used to save statistical data, to optimise and present offers on other websites.|At the time of visiting a page|1 year|Unique identifier|
|test\_cookie|The cookie checks if the user's browser supports cookies. Google DoubleClick network cookie.|At the time of visiting a page|Until the website window is closed|Unique identifier|
|fr|It is a pixel cookie that collects information about user behaviour on the website and is used to save statistical data and to optimise offers and to present them on Facebook network.|At the time of visiting a page|90 days|Unique identifier|
|tr|It is a pixel cookie that collects information about user behaviour on the website and is used to save statistical data and to optimise offers and to present them on Facebook network.|At the time of visiting a page|Until the website window is closed|Unique identifier|

**Profiling**

Profiling means any form of automated processing of personal data consisting of the use of personal data to evaluate certain aspects relating to a natural person, in particular to analyse or predict that natural person's professional experience, economic situation, health, personal preferences, interests, reliability, location or movements. Cgates can use profiling for the purpose of establishing creditworthiness, for marketing purposes, also in order to customise services provided to a customer, based on lawful interest, in performance of contractual terms or at the customer’s request in getting ready for conclusion of an agreement or with the customer’s consent.

**Cgates customers’ data protection**

Not only quality and safety of services, but also protection of customers’ data is important for Cgates. The customer, acting under the procedure established by legal acts, can access the customer's personal data processed by Cgates, including personal ID number. The customer, having produced his personal identity document, has the right to apply to Cgates for rectification of incorrect, incomplete or inaccurate data, to object to and/or withdraw his consent to processing of his personal data for direct marketing purposes.

Customers’ personal data are provided to third parties in cases provided for in Cgates agreement, rules and legal acts.

Cgates processes its customers’ data in accordance with Article 6(1)(a), (b) and (f) of Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (the General Data Protection Regulation, hereinafter referred to as the GDPR), and provisions of the Law of the Republic of Lithuania on the Legal Protection of Personal Data, the Law of the Republic of Lithuania on Electronic Communications, as well as other legal acts on data protection.

Cgates collects and processes your personal data for the following purposes: (a) the conclusion of service agreements for the provision of electronic communications and other services provided by the Company, in writing, by actions implying such conclusion or over the telephone, where one of the parties is the data subject (the data processed relates to provision of services related to television, internet and public fixed telephone communications services); (b) for the lawful interest pursued by the data controller or by a third party to whom the personal data are provided, where interests of the data subject are not overriding; (c) the data subject gives consent; (d) the collection of fees for services the customer is late to pay; (e) direct marketing, (f) recording of telephone conversations.

Personal data retention period (unless, in certain cases, indicated otherwise): personal data, if the customer paid for the services fully, shall be kept for 10 years after the date of termination of the services agreement. Personal data, where the customer did not pay for the services fully, shall be kept for 10 years after the debt is paid.

All information provided by the customer is stored on secure servers. All payments are encoded using industry standard technologies. If the customer has been given a password (or the customer chooses a password) for logging into Cgates website, the customer is responsible for not disclosing this password. The customer should not reveal this password to anyone. If possible, after each login, the customer should disconnect from all public or shared devices. If the customer returns or sells a device he could have used to access Cgates services, first of all, he must disconnect himself and disconnect the device. If the customer’s password or device is unsafe or the customer did not disconnect himself or did not disconnect the device, subsequent users can get access to the customer’s account and personal information. Online transmission of information is not yet fully secure. Despite all Cgates’ efforts, the Company cannot guarantee the security of the customer’s information and cannot assume responsibility for unauthorized access to this information. The Customer presents information on Cgates website at its own risk. Given that Cgates has access to customer's information, the Company uses strict methods and safety measures to prevent unauthorized access to the customer’s information. The customer’s personal information will not be processed longer than it is necessary. The extended retention period may be based on the customer’s consent, specified in the customer’s agreement or established by applicable legal acts.

**Linking the customer’s Cgates account to the customer’s social media account**

If it is technically possible for the customer to link his Cgates account to his social media account, for example, an account on the website of the social network Facebook, and if the customer decides to link his social media account to Cgates services, we will collect certain information about the customer’s social media account, and such information may be used in the above manner. Cgates shall not be responsible for the content of services that are not part of Cgates or for the information that the customer shares or sends through use of such services. If the customer uses third party services, the customer should read their privacy policy to find out how they will process information collected from the customer.

**Your rights**

The customer has the right to portability of his data transferred to Cgates (Article 20 of the GDPR). The customer can exercise this right by submitting a request directly upon arrival at Cgates customer service centre and presentation of the personal identity document. If a request is submitted by a representative, a representation document is also to be produced. If a request is submitted electronically, it must be signed with an electronic signature. If a request is sent by post, a copy of the personal identity document is to be attached. The customer has the right to receive personal data relating to him which he has provided to Cgates, in a structured, commonly used and machine-readable format.

The Customer has the right to file a complaint concerning processing of his personal data to the supervisory authority: State Data Protection Inspectorate, A. Juozapavičiaus St. 6, 09310 Vilnius, tel.: (+370 5) 271 2804, 279 1445, e-mail: <ada@ada.lt>.

You have the right to contact Cgates with a request to provide information about your personal data processed by Cgates.

If you are concerned about Cgates actions/omissions that may not comply with the requirements of this privacy notice or legal acts, you can contact Cgates in any way convenient to you:

- writing at the e-mail address: [info](mailto:bendraukime@bite.lt)@cgates.lt;
- making a phone call at No. +370 5 215 0000;
- sending a letter at Ukmergės St. 120, LT-08105 Vilnius;
- arriving at any Cgates customer service centre.
- for the implementation of the GDPR provisions, Cgates has appointed a Data Protection Officer (DPO), phone: +370 5 213 9079, <duomenu.apsauga@cgates.lt>.

**Changes to this Policy**

Cgates reviews the Policy regularly. If Cgates makes material changes to this Policy, we will inform you about this by publishing a relevant notice on Cgates website.
|<p>UAB Cgates </p><p>Legal entity code 120622256</p><p>VAT number LT206222515</p>|<p>Ukmergės St. 120, LT-08105 Vilnius</p><p>Tel.: +370 5 213 8822, fax: +370 5 213 7799</p><p>E-mail: <info@cgates.lt>, [www.cgates.lt](http://www.cgates.lt) </p>|<p>Bank account: LT18 7044 0600 0105 4164</p><p>Register of Legal Entities of the Republic of Lithuania</p><p>Registrar: state enterprise Centre of Registers</p>|
| :- | :- | :- |
