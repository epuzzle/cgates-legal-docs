﻿![](RackMultipart20210603-4-11y7wp1_html_674a7be6ce02648c.png)
**Revision of 1 March 2024**

**Cgates TV App Terms of Use**

**I. General Terms and Conditions**

1.	Cgates TV is an online service provided through registered devices (hereinafter referred to as the Service). By purchasing the Service, you (hereinafter referred to as the Customer, you, your, yours) will receive access to and will be able to watch TV channels, films, series live or subsequently streamed content in streaming format. You can get acquainted with the up-to-date information about our current service packages on our website at www.cgates.lt or via the Cgates TV smart application (hereinafter collectively referred to as the Website). 
2.	The Service is offered by UAB Cgates, legal entity code 120622256, registered office address Ukmergės st. 120, Vilnius, tel. No. (8 5) 215 0000, e-mail address info@cgates.lt. 
3.	These Terms of Use (together with the documents referred therein) shall apply to and shall be legally binding on all users of the Service. You must read and agree to these Terms of Use before using the Service. By using the Service, you hereby agree to these Terms of Use and to be bound by them. If at any time you do not agree to the Terms of Use or are unable to comply with them, you may not use the Service. You may at any time review the Terms of Use stored on the Website at https://www.cgates.lt/pagalba-klientams/duk/duk-sutartys/. These Terms of Use, together with the Privacy Policy and the Cookie Policy, as well as the Special Terms and Conditions, the Equipment Terms (if applicable), form an integral part of the Service Provision Agreement (hereinafter referred to as the Agreement) concluded between Cgates and you and supersede and replace any prior written or oral agreements with respect to the subject-matter hereof.
4.	The Terms of Use have been drawn up in accordance with the Republic of Lithuania Law on Electronic Communications, the Republic of Lithuania Law on the Provision of Information to the Public, the Rules for the Provision of Electronic Communications Services and other normative legal acts of the Republic of Lithuania.

**II. Terms**

5.	Terms used in the Terms of Use:

5.1. Customer shall mean a natural person who has signed the Cgates Service Provision Agreement and uses the electronic communications services provided by Cgates to meet personal, family or household needs not related to business or profession;

5.2. Service shall mean one of the services provided on the Cgates network, for the provision of which the Customer has entered into a Cgates Service Provision Agreement;

5.3. Agreement shall mean the Cgates Service Provision Agreement concluded between the Customer and Cgates, the summary of the Cgates Service Provision Agreement provided to the Customer, the publicly available rules, the Terms of Use of this Service and the rates for the Services, the Service orders, and any other annexes, amendments and supplements to the Agreement, which, after the signing of such annexes, amendments and supplements, are available for the Customer to access in the Customer's Self Service. The Agreement shall be concluded upon the Customer's signing of the Cgates Service Provision Agreement or its annex (the order) in the form set out by Cgates, or upon any other agreement between the Parties on the provision of the Services (e.g., verbally or by affirmative action). The arrangement of the Parties shall become part of the Agreement from the moment when the Services ordered by the aforementioned Agreement or its Annex (order) are actually provided to the Customer;

5.4. Cgates Network Endpoint shall mean an endpoint of the Cgates Network to which equipment owned by the Customer or transferred by Cgates to the Customer is connected.

5.5. Cgates Equipment shall mean the equipment (including software) required to receive the Services provided by Cgates and provided to the Customer as set out in the Terms of Use, the Agreement and/or any annexes thereto;

5.6. Minimum Term of Service shall mean the Customer's commitment to use the Services and/or Equipment for a specific period of time specified in the Agreement and/or annexes thereof (orders), which shall commence from the commencement of the provision of the Services and at the end of which the discounts granted to the Customer shall cease to apply;

5.7. Basic TV Package shall mean a package of TV programmes consisting of a minimum number of TV programmes and provided to all Customers for a minimum fee set by Cgates. In the event of uncertainty as to which of the TV packages distributed by Cgates is the basic package, the package containing the smallest number of TV programmes and offered at the lowest price of all the packages offered and meeting the following conditions shall be considered the basic package: (i) it includes mandatory retransmission of all uncoded national television programmes broadcast by Lithuanian National Radio and Television and other television programmes which have been granted the status of mandatory retransmission by the Lithuanian Radio and Television Commission; (ii) more than 50% of the television programmes included in the package are broadcast in the official language of the European Union. The composition of the Basic TV Packages may differ between different technologies and/or between different categories of users;

5.8. Additional Services shall mean additional services ordered by the Customer (e.g. retransmission of additional sets of TV programmes, rental of equipment, recording of programmes, etc.), the provision of which is directly related to the Services and which cannot be provided separately from the Services;

5.9. Additional TV Packages shall mean the TV packages that may only be ordered together with the Basic TV Package for an additional fee;

5.10. Activation of the Services  shall mean the work carried out on the Cgates Network/systems and/or on the Customer's property/equipment in order to create the necessary technical conditions required for the provision of the Services;

5.11. Service Commencement Date shall mean the time from which the Customer shall be able to commence using the booked Services as set out in the Terms of Use;

5.12. Cgates Network shall mean the electronic communications network owned or operated by Cgates.

**III. Ordering, Activation and Provision of the Services**

6.	The Customer may order the Services on the basis of the Agreement, the Terms of Use or by the methods published by Cgates. Regardless of the method of ordering, the Customer undertakes to pay for the Cgates Services and any other fees charged by Cgates.
7.	The Services ordered by the Customer shall commence no later than 10 business days from the date of ordering the Services, provided that the Customer complies with the specified conditions. CGATES TV can be ordered and managed by downloading the CGATES TV mobile/TV application to your device. The Customer must be at least 18 years of age at the time of ordering CGATES TV. For CGATES TV to work, internet access (at the speed of at least 1.5 Mbps) and a device (TV, smartphone/tablet, etc.) that meets the requirements specified on the application purchase website must be available.
8.	Troubleshooting of the Cgates Network shall be carried out within 16 business hours from the moment of notification of the fault, or at a time pre-agreed with the Customer, if it is necessary to travel to the place of service provision in order to identify the fault and to fix it.
9.	The time taken to rectify faults shall commence from the moment the fault is reported. For faults reported after the end of a business day, at weekends and on public holidays, the time taken to rectify the fault shall start at 8:00 of the next business day. The term of rectification of faults shall not include weekends, public holidays and faults reported after the end of a business day.
10.	The rectification of faults shall be carried out during business hours, i.e. from Monday to Friday at 8:00 – 17.00, with the exception of cases agreed upon separately.
11.	Additional TV Packages may only be ordered after ordering the Basic TV Package. Additional TV services may only be ordered after subscribing to the Basic TV Package. In the event of changes in market conditions or applicable legislation, as well as for other objective reasons (e.g. circumstances related to copyright protection, decisions of supervisory and other authorities, contractual obligations, acts or omissions of suppliers, other commercial reasons, etc.), Cgates shall have the right to modify the content of the Basic TV Package or the Additional TV Packages unilaterally and without adverse consequences. Cgates shall inform the Customer of such changes promptly by means of a self-service notification or any other reasonable means.
12.	If the Customer enters into a distance or off-premises contract, the Customer shall have the right to withdraw from the Agreement within fourteen days. By entering into a distance contract, the Customer hereby confirms that he/she wishes the services to be commenced before the expiry of the fourteen-day withdrawal period, and the Customer is hereby informed and agrees that in such case, upon termination of the Agreement, he/she shall be liable to pay to Cgates an amount of money equal to the price of the services actually provided.

**IV. Obligations of the Parties**

13.	Cgates undertakes:

13.1.	To provide the Services ordered by the Customer under the terms and conditions set out in the Agreement to the end point of the network owned by Cgates.

13.2.	To advise the Customer on the provision of the relevant services, and on the hardware and software required to receive and complete the service during the period of Service provision. The Customer may get acquainted with all the information about Cgates' services, the procedure for their provision, terms and conditions, fees, liability of the Parties to the Agreement, and other information related to the provision of the services on the website www.cgates.lt, or by calling the telephone numbers specified in the Agreement.

13.3.	To comply with the requirements set out in the Service Provision Agreement, these Terms of Use, the Annexes to the Agreement (orders), and the requirements of the legal acts of the Republic of Lithuania regulating electronic communications activities when providing the Services ordered by the Customer.

14.	The Customer undertakes:

14.1.	To pay for the Services in a timely and proper manner. This is considered to be a material term of the Agreement. 

14.2.	Not to transfer the Services and/or the Cgates Equipment to any third party without the consent of Cgates.

14.3.	To use the Services in the manner and for the purposes set out in the Agreement and these Terms of Use. Not to use the Services for illegal purposes, in violation of the rights of other Cgates customers, the laws of the Republic of Lithuania and the rights of third parties, not to undermine the integrity and functioning of the Cgates Network, not to behave aggressively in relation to the employees or authorised persons of Cgates, and not to comply with the lawful requirements of Cgates, which are necessary to ensure the proper provision of the Services. This is considered to be a material term of the Agreement.

14.4.	The Service shall be available in Lithuania, as well as in EU and EEA Member States when you are visiting them temporarily. Cgates shall not be liable for any costs, violations of law or otherwise, including, for example, any liability incurred by you if you use or attempt to use the Service outside of Lithuania and the EU and EEA Member States.

14.5.	To report and not arbitrarily correct any fault of the Services. The Customer may register faults and/or request assistance/information about the Services free of charge by calling the telephone numbers specified in the Agreement. 

14.6.	To use the Services for a period that is not less than the Minimum Term of Service, if any. The minimum term for ordering an Additional TV Package shall be 1 month. The minimum period for ordering the Services by prepayment method shall be 1 month. If the Customer orders to Additional Services, the Minimum Term of Service shall not be extended unless the Customer expressly confirms his/her consent to the extension of the Minimum Term of Service when ordering Additional Services.

14.7.	To inform Cgates in writing or by telephone of any change in your data specified in the Agreement immediately, but no later than within 5 (five) calendar days of the date of the change.

14.8.	Not to use the Service for activities prohibited by the laws of the Republic of Lithuania, to send computer viruses, mass malicious information (spam), or for any other purposes that may violate the rights and legitimate interests of third parties.

14.9.	If the Customer is in debt to Cgates, the Customer shall pay all costs associated with the collection of the debt, including but not limited to the debt administration fee and any other costs incurred by Cgates in connection with the debt collection.

14.10.	To protect and not disclose to any third party any password or other information provided to the Customer for the purpose of ordering and managing the Services.

14.11.	Not to distribute, copy, reproduce, publish, rebroadcast, broadcast, transmit to third parties or otherwise make available to the public (including via the Internet) the Cgates rebroadcast programmes or any other works displayed.

**V. Rights of the Parties**

15.	Cgates shall have the right:

15.1.	To change the terms and conditions of the Agreement in the event of changes in the legislation of the Republic of Lithuania, market conditions or other significant economic conditions, as well as in the event of any other objective reasons, upon 1 month's prior notice in writing (by means of a notice in the self-service) or by any other means of communication to the Customer. If the Customer does not agree to a change in the terms and conditions of the Agreement, the Customer shall have the right to unilaterally withdraw from the Services and to terminate the Agreement before the expiry of the Minimum Term of Service at no additional cost to the Customer, unless the proposed changes are in the Customer's favour, of an administrative nature only and without any adverse effect on the Customer, or in compliance with the requirements of the law directly provided for in the law of the European Union or of the Member State of the European Union. In such a case, the Customer must notify Cgates in writing or by e-mail prior to the date of the planned change. In such a case, the Customer shall not be subject to any commitment concerning the Minimum Term of Service. Notwithstanding the foregoing, Cgates shall have the right, at any time during the term of the Agreement, to propose to the Customer to amend or supplement the Agreement.

15.2.	If the Customer fails to comply with the payment procedures provided for in the Agreement or its annexes or commits other material violations of the Agreement or its terms and conditions, Cgates shall have the right to restrict, suspend or terminate the provision of the Services by means of a text message sent to the telephone number or e-mail address indicated by the Customer at least 5 days in advance. Upon payment of the arrears by the Customer, Cgates undertakes to resume the provision of the Service to the Customer on the next business day following receipt of the information on full settlement with Cgates. Upon renewal of the Services, Cgates shall be entitled to require the Customer to pay the renewal fee set by Cgates. The date of receipt of information of full settlement with Cgates shall be deemed to be the date on which the Customer submits to Cgates a document proving payment or the Customer's amounts payable are credited to Cgates' account. If the Customer fails to remedy the violations of the Agreement within 30 days, Cgates shall be entitled to terminate the Agreement. 

15.3.	To require the Customer to pay one month's payment in advance for the Services if the Customer is in arrears to Cgates for the provision of the Services.

15.4.	If the Customer arbitrarily attempts to change or connect to a new service (attempting to hack into the Cgates system, change its configuration, false representations, etc. ), as well as in cases where the Customer's computer(s) or other equipment belonging to the Customer is spreading a virus stream that prevents Cgates from providing a quality Internet connection to the Customer(s), or the Customer is late in making payments for the Services, or the Customer arbitrarily transferred the Services to third parties, or when there is an obvious threat of a security incident or a security incident is detected, Cgates shall have the right to temporarily suspend/restrict the provision of the Service and to demand indemnification from the Customer for the damage caused. Suspension/restriction of services by Cgates on any of the grounds set out in this Clause shall not relieve the Customer of its obligation to pay the service fee(s).

15.5.	Cgates shall have the right to collect and provide information about the Customer's non-payment to third parties authorised by law to process personal data for the purpose of debt collection. It may also require the Customer to reimburse all reasonable costs associated with the collection of the debt for the Services provided.

15.6.	To temporarily suspend the provision of Services to the Customer after duly informing the Customer thereof by e-mail and/or telephone and/or SMS and on the website www.cgates.lt due to the repair, installation and preventive maintenance work being carried out.

15.7.	Cgates shall have the right to make updates to the Cgates software and/or necessary changes to the settings to ensure the security and/or proper functioning of the Services. In such cases, Cgates shall not be liable for the removal or modification of any settings made by the Customer on the Equipment, or for any risks or damages that the Customer may incur as a result.

15.8.	To restrict the provision of Services in cases where Cgates does not receive or receives incorrect/incomplete information about the Customer's payment due to the fault of the Customer or the bank or other tax administration companies. In such cases, payments for the Services may be treated as unpaid until the date of receipt of the relevant information. Such restriction on the provision of Services shall be reinstated within 1 business day of receipt of correct/complete payment information.

15.9.	To contact the Customer using the contact details set out in the Agreement and to inform the Customer of any changes to the Terms of Use.

16.	The Customer shall have the right:

16.1.	To withdraw from the Cgates Services by terminating the Agreement in accordance with the procedures set out in these Terms of Use.

16.2.	To suspend the Cgates Agreement upon payment of the fees set by Cgates and in accordance with the procedures set by Cgates. The minimum duration of the suspension shall be 1 month and the maximum total duration of the suspension shall be 6 months. The suspension shall apply no more than once per calendar year. In such a case, the Minimum Term of Service shall be extended by the number of months for which the Agreement was temporarily suspended. Upon expiry of the suspension period, the provision of the Service(s) shall resume automatically.

16.3.	To receive an indemnification for disruptions to the Service caused by Cgates in accordance with the terms and conditions set out in the Agreement.

16.4.	To choose, at his/her sole discretion, the payment methods and procedures offered by Cgates for the Services. To pay for the Services provided in advance.

16.5.	After ordering the Cgates Smart TV Service, the Customer shall have the right, subject to the terms and conditions set by Cgates and for the period of time set by Cgates (full terms and conditions are available at www.cgates.lt), to order for his/her own personal use the programme recording and other interactive services. Cgates shall record and reproduce the TV broadcasts for the Customer's personal use upon request. The Customer shall be solely responsible for the lawfulness of its data uploaded to the Cgates repository. The Customer must pay for the additional chargeable services he/she has ordered. 

16.6.	If the equipment transferred to the Customer by Cgates on the basis of the Equipment Purchase and Sale Agreement is not suitable for the Customer, the Customer shall have the right to return the equipment within 14 days of purchase. The right of withdrawal from the Equipment Purchase and Sale Agreement shall be exercised in accordance with the terms and conditions set out in the Equipment Purchase and Sale Agreement. The Equipment shall be returned through a Cgates representative in accordance with the terms and conditions set out in the Equipment Purchase and Sale Agreement. 

**VI. Arrangements for the Payment of Services**

17.	The Customer shall pay for the Services in accordance with the rates for the Services set out in the Agreement or its Annexes. The reference period shall be one calendar month. There shall be no credit limit for the Services unless otherwise agreed upon by the Parties. Additional fees shall apply when ordering the following: (i) detailed payment summary (7 months to 24 months inclusive) EUR 5.00; (ii) detailed payment summary (over 24 months) EUR 20.00.
18.	Payment for the CGATES TV service can be made by prepayment or payment after the service has been provided. Depending on the payment method used for ordering the CGATES TV service, the Customer shall be billed for the preceding calendar month or the invoice shall be made available for prepayment by sending the invoice (or a link to the invoice) electronically to the e-mail address provided by the Customer or by submitting it on the self-service website before the middle of the current month, or before the ordering of the CGATES TV service, which shall be payable by the last day of the current month or before the CGATES TV service is ordered, or, if the invoice specifies a later date, by the date specified on the invoice. Upon payment of the invoice submitted, the Customer hereby confirms the services ordered. Failure to receive an invoice through no fault of Cgates shall not relieve the Customer of the obligation to pay for the Services.
19.	Failure to pay the service fee within the above deadlines may result in interest on arrears of 0.02% for each day of delay. A debt management fee shall apply if a Customer is in arrears with payment for services.
20.	Cgates shall invoice the Customer for the services provided free of charge by electronic means, i.e. in the Customer's self-service. If the Customer wishes to receive Cgates' invoice for the Services by unregistered mail, the Customer shall pay the fee for sending a paper invoice as indicated on https://www.cgates.lt/pagalba-klientams/duk/duk-sutartys/. Failure to receive an invoice through no fault of Cgates shall not relieve the Customer of its obligation to properly settle for the Services. For the Services provided, Cgates shall issue and submit to the Customer, by the 10th day of each month, invoices for the previous accounting period, which must be paid by the last day of the current month. If the invoice specifies a later date, the payment must be made by the date specified in the invoice. In the event of a change in the VAT rate, all payments under the Agreement for the Services shall be recalculated accordingly. Any changes in fees provided for in this Clause of the Agreement shall be calculated irrespective of whether or not the Minimum Term of Service is set.
21.	With the first invoice, the Customer shall pay the service activation fee set by Cgates and for the Services actually provided. Failure to pay the service fee within the above deadlines may result in interest on arrears of 0.02% for each day of delay. In the event of termination of the Agreement, Cgates shall not refund the activation fee or any fees paid for the Services.
22.	The date of commencement of the Services shall be included in the billing period for which payment is due. The procedures set out in this Clause shall also apply in the event of termination of the Services or suspension of the Agreement.
23.	If the payment received from the Customer is not sufficient to cover the entire amount owed for the Services, the payment received shall be distributed as follows: the costs incurred by Cgates in collecting the debt from the Customer shall be paid first, followed by the accrued interest on arrears and/or interest, and then by the oldest outstanding debt. In the event that, due to technical feasibility, the Customer's contributions have been used to settle a disputed debt, Cgates shall reimburse the Customer for the late payment surcharges applied and for the disputed debt, or any part thereof, in the event that the dispute is resolved in favour of the Customer in full or in part.
24.	Upon termination of the Agreement and/or cancellation of the Service, the Customer shall be obliged to pay Cgates for the Services provided until the date of cancellation of the Services. Unless the Minimum Term of Service has expired, the Customer shall pay all charges relating to the Services actually provided, as well as any activation or other charges imposed by Cgates (if any have not been paid), any discounts granted, and any other costs and expenses incurred by Cgates in the performance of the Agreement prior to the date of termination. In the event of termination of the Agreement due to the fault of Cgates, the Customer shall be obliged to pay all fees related to the Services actually and properly provided, as well as the activation fees for the Services.

**VII. Conditions for Cancellation and Termination of Services**

25.	If the Customer enters into a fixed-term service agreement with Cgates, which provides for a Minimum Term of Service, the Customer may cancel the Services by giving Cgates not less than 3 months' notice in advance and not more than 5 (five) business days' written notice. Once the Customer has entered into a pre-paid, open-ended contract, the Customer may cancel the CGATES TV service at any time. This can be done by logging in to your Cgates user account. In all cases of cancellation/termination by the Customer, the provisions of Clause 23 of these Terms of Use shall apply.
26.	Cgates may unilaterally, without recourse to court, terminate the Agreement or one of the services ordered by the Customer by giving the Customer at least 5 (five) business days' prior written notice if the Customer violates the material terms of the Agreement, the present Terms of Use or any other annexes to the Agreement, and for other objective reasons. The material terms of the Agreement shall in all cases be deemed to be those set out in Clauses 14.1 to 14.11 of these Terms of Use. 

**VIII. Liability of the Parties**

27.	Cgates shall not be liable for the Customer's inability to use the Services and/or any damage caused to the Customer where this is not due to Cgates' fault.
28.	By using the Services, the Customer shall be fully liable for his/her own actions and the actions of other persons who use the Services provided to the Customer, as well as for any damages caused to Cgates or third parties.
29.	The Customer shall be fully liable for the content of any information that it provides or receives through the Cgates Services, including, but not limited to, violation of laws governing intellectual property, protection of personal data, provision of advertising services, etc.
30.	If the Customer violates the provisions of these CGATES TV Terms of Use, Cgates shall have the right to restrict the Customer's use of the CGATES TV Services in whole or in part. Cgates shall have the right to restrict or suspend the provision of Services immediately and without prior notice if the Customer violates the following material terms of use of the Services:

30.1. the use of CGATES TV violates the rights and legitimate interests of third parties and the principles of public order;

30.2. the Customer records, copies or otherwise reproduces TV programmes or other content obtained through the Services without the permission of Cgates;

30.3. without the permission of Cgates, the Customer publicly publishes (broadcasts, rebroadcasts, makes available to the public via the Internet), distributes to the public, or in any way other than as provided for by the present Terms of Use uses the programmes of CGATES TV, or any part of the programmes, or any other content obtained from the Services;

30.4. The Customer shall be prohibited from performing any act that attempts to hack into the CGATES TV Application, and any person who violates this prohibition shall be liable to the penalties provided by law. In the event of a violation, Cgates shall have the right to bring an action for damages against the infringer promptly and without prior notice;

30.5. The Customer undertakes not to disclose the CGATES TV account login details to any third party. For security reasons, third parties shall not be allowed to access the Customer's account. In the case of a violation committed through the use of a specific account, the person who created the account shall be liable for the violation and any damages resulting therefrom.

30.6. if the Customer is given access to the CGATES TV service in EU countries, such access must be of a temporary nature. In the event of abuse, Cgates may request additional information and may restrict or suspend services in the absence of such information.

31.	The Customer undertakes to indemnify Cgates, the holders of copyright and related rights, as well as any other third parties, against any damage caused by the Customer's actions.
32.	The Parties shall be exempted from liability for non-performance or improper performance of their obligations under the Agreement due to force majeure in accordance with the procedure established by the legislation of the Republic of Lithuania.
33.	A Party must fully indemnify the other Party for direct damages if the damage is caused by the Party's fault.
34.	Neither Party shall be liable for indirect or consequential damages to the other Party.

**IX. Data Protection**

35.	Cgates is not only interested in the quality and security of our services, but also in the protection of Customers' data. By signing the Cgates Agreement, the Customer hereby confirms that he/she is aware of the right to know (be informed) about the processing of his/her personal data, to have access to his/her personal data and how they are processed, to request correction or destruction of his/her personal data in the cases and in accordance with the procedure set out in these Terms of Use, the Law on Legal Protection of Personal Data and other legal acts, and to object to the processing of his/her personal data. The Customer shall have the right to request that Cgates rectify incorrect, incomplete or inaccurate data, as well as the right to object to and/or revoke the right to have personal data processed for the purpose of direct marketing, by submitting a document confirming his/her identity.
36.	Cgates shall process Customer data in accordance with Article 6(1)(a), (b) and (f) of Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (General Data Protection Regulation) (hereinafter referred to as GDPR), as well as Article 6(1)(a), (b), and (f) of the Republic of Lithuania Republic of Lithuania Law on Legal Protection of Personal Data, and of the Republic of Lithuania Law on Electronic Communications, as well as with the other provisions of other legal acts regulating data protection.
37.	The detailed provisions relating to the protection of personal data are described in the CGATES TV Privacy Policy, which is made publicly available on the Cgates website www.cgates.lt, the Cgates customer self-service and/or the Cgates application.  
38.	Cgates has appointed a Data Protection Officer (DPO) to implement the provisions of the GDPR. E-mail of the DPO: duomenu.apsauga@cgates.lt.
39.	During the term of the Agreement, Cgates shall have the right to provide the Customer with information about the Cgates Services, their features, updates, as well as discounts or exclusive offers, and other information related to the Services, using the Customer's personal data for this purpose, including, but not limited to, the Customer's name, surname, telephone number, e-mail address. The Customer's personal identification number shall not be processed for this purpose. You can unsubscribe from the notifications described above at any time by contacting Cgates by tel. No. at (8 5) 215 0000 or by e-mail at duomenu.apsauga@cgates.lt. In any event, during the term of the Agreement, Cgates shall have the right to contact the Customer by telephone, SMS, e-mail or self-service message regarding the performance of the Agreement.
40.	The Customer shall have the right to lodge a complaint regarding the processing of his/her personal data with the supervisory authority: the State Data Protection Inspectorate (Valstybinė duomenų apsaugos inspekcija), L. Sapiegos g. 17, Vilnius, tel. No. (8 5) 271 2804, 279 1445, e-mail ada@ada.lt.

**X. Final Provisions**

41.	These Terms of Use shall apply from 1 March 2024. The Terms of Use is a publicly available document published on the Cgates website www.cgates.lt, the Cgates Customer Self-Service and/or the Application.  Cgates draws the Customer's attention to the fact that the Terms of Use may be downloaded by the Customer to his/her personal storage medium for the purposes of documentation, reproduction without modification, and future review. Cgates shall have the right to modify these Terms of Use by notifying users via the Cgates Customer Self-Service or any other reasonable means and by posting the new version of these Terms of Use on the Cgates website www.cgates.lt. 
42.	All proprietary rights, including intellectual property rights, in and to the programmes and any related technical documentation, trademarks and other marks, copyrights, and other proprietary rights made available to Customer by Cgates shall remain the property of Cgates or its licensors.
43.	Neither Party may assign to a third party any of its rights or obligations arising out of the performance of the Agreement without the prior written consent of the other Party, except for the assignment of rights and obligations to a legal successor in title. It shall not be considered a violation of this provision if Cgates, in the performance of the Agreement, delegates certain functions or works to third parties while remaining fully liable to the Customer for the third parties.
44.	Disputes relating to non-performance or improper performance of the Agreement shall be settled by agreement between the Parties, and in the absence of agreement between the Parties – in accordance with the procedure established by the laws of the Republic of Lithuania. The Customer is hereby informed that he/she may apply to the entity that settles disputes between consumers and providers of electronic communications services in a non-judicial procedure, i.e. the Communications Regulatory Authority of the Republic of Lithuania (Mortos st. 14, Vilnius, tel. No. (8 5) 210 5633, fax (8 5) 216 1564, e-mail: rrt@rrt.lt, website: www.rrt.lt), or by filling in the application form on the Electronic Consumer Dispute Resolution Platform at https://ec.europa.eu/odr/. Information on the procedure for resolving disputes arising out of non-performance or improper performance of the Agreement shall also be publicly available on the Cgates website at www.cgates.lt.
45.	If any of these Terms of Use are declared invalid or inapplicable by the laws of the Republic of Lithuania, the other provisions of these Terms of Use shall continue to be valid and applicable.

Supplier of the CGATES TV service:

UAB Cgates, Ukmergės st. 120, LT-08105 Vilnius, Lithuania

legal entity code 120622256

Customer service by telephone +370 5 215 0000

Website [www.cgates.lt](http://www.cgates.lt/), e-mail info@cgates.lt 


I would like to receive notifications about services, offers and discounts:


<input type="checkbox" id="consent_sms_email" name="consent_sms_email" /> <label for="consent_sms_email"> By SMS </label>

<input type="checkbox" id="consent_call" name="consent_call" /> <label for="consent_call"> By Call </label>
