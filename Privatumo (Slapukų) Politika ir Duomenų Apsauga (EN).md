﻿**CGATES TV MOBILE APPLICATIONS PRIVACY POLICY**
Ver. 2, valid from 30/01//2024

**General Information**

This Privacy Policy (hereinafter referred to as the Policy) for the Cgates TV mobile application (hereinafter referred to as the App) describes the principles governing the collection, use and protection of the personal data (hereinafter referred to as the Personal Data) of users of the App (hereinafter referred to as you, your, yours, etc.), depending on the context. For more detailed information, please contact the contacts listed in the Policy.

UAB Cgates (depending on the context, referred to as the Data Controller, Cgates we, our, us, or the like) is the controller of the Personal Data we process about you when you use the App.

Details of the Data Controller:
UAB Cgates
Legal entity code: 120622256
Registered office address: Ukmergės st. 120, LT-08105 Vilnius
Data Protection Officer's e-mail address: duomenu.apsauga@cgates.lt

We shall apply the Policy: when you use the App; when we start processing Personal Data before the Policy came into force. We shall process your Personal Data in accordance with the General Data Protection Regulation 2016/679 (hereinafter referred to as GDPR), the Law on Legal Protection of Personal Data, the Law on Electronic Communications, other applicable laws and the Policy.

By using the App and providing us with Personal Data through the App, you hereby confirm that it is true and accurate and that you have read and agree to be bound by the Policy.

Terms used in the Policy shall have the same meaning as defined in the GDPR, unless otherwise specified in the Policy.

You may use the App (1) by entering into a contract with us as a customer, or (2) by being a subscriber where another person has entered into a contract with Cgates. To a limited extent (you can see what content is available on the App without the right to view it), you may use the App by visiting the App as a user without entering into a contract with Cgates. In all such cases, the provisions of this Policy shall be followed. If you allow another user to use the App under a contract between you and Cgates, you must also ensure that the user is acquainted with the Policy.

Persons under the age of 14 shall be prohibited from submitting their Personal Data to the App unless the consent of their parents or other legal representatives has been obtained prior to the submission of the Personal Data and the consent has been provided to us in advance. Persons over the age of 14 but under the age of 18 must have the consent of their parents or other legal representatives before submitting their Personal Data to the App and must promptly provide such consent upon our request. If you provide Personal Data that is not your own, you must have the consent of the person whose Personal Data you are providing and you must provide such consent promptly upon our request.

Cgates may change the Policy without prior notice to you. If Cgates makes material changes to the Policy, we shall notify you thereof.


**Technical Requirements for Using the App**

The App shall be available on the following devices:
1.	On Android and iOS smartphones and tablets – by downloading the Cgates TV application;
2.	On Amazon Fire TV – by downloading the Cgates TV application;
3.	On computers, using a web browser, using the link https://cgates.tv;
4.	On TVs with Android TV – by downloading the Cgates TV application;
5.	On TVs without the Android TV operating system – by using the CGATES TV Android multimedia device.

The App can be downloaded from:
1.	Huawei App Gallery;
2.	Google Play Store;
3.	Amazon Store;
4.	Apple App Store;
5.	Samsung App Store;
6.	LG App Store.

For the App to work properly, the following operating system version must be installed on the device:
1.	ANDROID TV1 APP: Android TV 7.0+;
2.	AMAZON FIRE TV Stick: FireOS 6+;
3.	ANDROID phone: android 7.0+;
4.	ANDROID tablet: Android 7.0+;
5.	APPLE phone (iPhone): iOS 13+;
6.	APPLE tablet (iPad): iOS 13+;
7.	PC CHROME browser: Chrome 81+ (latest 2 versions);
8.	PC MICROSOFT browser: Edge 40+ (latest 2 versions);
9.	PC FIREFOX browser: Firefox 81+ (latest 2 versions);
10.	PC OPERA browser: Opera 52+ (latest 2 versions).
1 TV manufacturers which have models with the Android TV operating system:
1.	Xiaomi TV (Android v7 and above);
2.	Nokia TV (Android v7 and above);
3.	Sony TV (Android v7 and above);
4.	Philips TV (Android v7 and above);
5.	Hisense TV (Android v7 and above);
6.	Siemens TV (Android v7 and above).


**Terms**

**Customer** - shall mean a natural person who has signed the Cgates Service Provision Agreement and uses the electronic communications services provided by Cgates to meet personal, family or household needs not related to business or profession.

**Personal Data** - shall mean any information relating to a natural person, i.e. the data subject, who is known or identifiable, directly or indirectly, by reference to the data concerned.

**Services shall** - mean any products and/or services offered by Cgates, whether electronic or non-electronic.


**What data do we process, for what purpose and on what basis, and how long do we retain it?**

Purpose of data processing	Personal data processed	Legal basis	Retention period
Creating and using an App account	E-mail address, telephone number, name, surname, city, password, account profile preferences	Contract execution	Personal Data shall be retained for 10 years from the end of the calendar year of termination of the Service Agreement, provided that the customer has paid in full for the Services.
Personal Data where the customer has not fully paid for the Services shall be retained for 10 years from the end of the calendar year of cancellation of arrears.
Performance of the Service Agreement and accounting	Content usage data (movies, channels, shows, recordings, viewing statistics, personalised content recommendations, date and time, duration), IP address of the device, information on the type of viewing device, customer identification number, paid services ordered through the App and the price of the paid services, data on content usage recorded in accounting records, data recorded in the accounting and payment records for services	Contract execution
	Personal Data shall be retained for 10 years from the end of the calendar year of termination of the Service Agreement, provided that the customer has paid in full for the Services.
Personal Data where the customer has not fully paid for the Services shall be retained for 10 years from the end of the calendar year of cancellation of arrears.
Administration of technical support requests	Data recorded during troubleshooting and servicing of the App (inquiry, telephone call recording, data recorded during maintenance), e-mail address, tel. No., content usage data	Contract execution
Cgates' legitimate interest in capturing evidence of proper contract performance	Personal Data shall be retained for 10 years from the end of the calendar year of termination of the Service Agreement, provided that the customer has paid in full for the Services.
Personal Data where the customer has not fully paid for the Services shall be retained for 10 years from the end of the calendar year of cancellation of arrears.
Management of complaints	Information contained in the complaint or inquiry	Legal obligation	Pending the final resolution of the complaint and for 10 years after the end of the calendar year in which the complaint is finally resolved.
Debt administration	Details of the debt, administration and collection thereof	Legitimate interest of the Data Controller in collecting a debt for the Services	10 years from the end of the calendar year of cancellation of the debt.
Ensuring the technical functioning of the App	Information about the use of the App collected through cookies.
See more in the "Cookies" section of the Policy	Legitimate interest of the Data Controller in the proper functioning of the App or consent	In the "Cookies" section of the Policy
Defending of Cgates' rights before public authorities or the courts (if necessary)	Personal Data held by Cgates that may be necessary for the exercise of Cgates' rights	The legitimate interest of the Data Controller in proving the proper provision of the Services or defending rights	Until a final and non-appealable decision has been rendered and 10 years after the end of the calendar year in which the claim is finally settled.
Provision of personalised offers (profiling for marketing purposes)	Details of your use of App services, e-mail address, telephone number	Consent	Until you withdraw your consent
Business analytics	Data about the use of App services	The legitimate interest of the Data Controller in improving products and services and expanding activities	During the planning and implementation of business development activities

**Is it necessary to provide Personal Data?**

Personal Data is required for the creation and use of an App account, for the performance of the Service Agreement and for accounting purposes if you are using the Cgates TV paid services and you have entered into a contract with Cgates in order to use the full functionality of the App. It is not necessary to provide your Personal Data for other processing purposes, but you may not be able to use parts of the App Services.

**Information from third parties**

Cgates may supplement the information it receives from Customer and information about Customer's use of the Cgates Services with publicly available information about Customer, as well as information Cgates receives from third parties or through cookies or similar means. The information generated by cookies about your use of the App Services may be linked by Cgates to other Personal Data held by Cgates about you and used for business analytics and profiling purposes through your e-mail address and Customer ID number.

**What do we do to protect your information?**

In order to protect Personal Data collected through the App, Cgates has implemented appropriate organisational and technical security measures to protect Personal Data from accidental or unlawful destruction, alteration, disclosure or any other unauthorised processing.

The Customer shall be responsible for not disclosing the App Account login password. Where possible, after each login, the Customer should log out of the App account on all devices that are public or shared. If the Customer returns or sells the device through which he/she was able to use the Cgates Services, the Customer must first log out of the Account on the device. If the Customer's password or device is insecure, or the Customer has not logged out of the Account on the device, subsequent users may be able to gain access to the Customer's account and personal information.

The transmission of information over the internet is not completely secure. Despite Cgates' best efforts, we cannot guarantee the absolute security of Customer's information or accept liability for possible unauthorised access to such information. The Customer shall submit information to the App at his/her own risk. Given that Cgates has access to Customer's information, we shall use security measures to prevent unauthorised access to Customer's information.


**Who do we receive your Personal Data from?**

We receive your Personal Data directly from you, generated by your use of the App and/or we may receive it from third parties such as:
•	Payment service provider partners (banks, payment institutions);
•	Competent governmental or law enforcement authorities ordering us to take certain actions in relation to you or your Personal Data.

**Who do we disclose your Personal Data to?**

We shall disclose your Personal Data:
•	When we provide the Services in cooperation with partners, i.e. our partners acting as data processors (e.g., content partners receive only a customer identification number without any other information that can directly identify the customer; data hosting, IT system support providers; telephone support agencies) or independent data recipients (partners providing payment administration or other services related to the Services you have requested; other persons providing necessary services such as lawyers, auditors, consultants, etc.). These recipients shall have limited ability to use your Personal Data. They may not use this information for purposes other than to provide services to us. We shall only provide these service providers with as much of your Personal Data as is necessary to provide the specific service.
•	To government or law enforcement authorities or other entities, in accordance with applicable law, where required to do so by law or as necessary to protect our rights.


**Provision of personalised offers (profiling for marketing purposes)**

For the purpose of direct marketing, we shall carry out profiling (i.e. automated processing of personal data where personal data is used to evaluate certain personal aspects relating to an individual and to classify App users into certain categories). Profiling shall not give rise to legal consequences or similar significant effects for you. We shall process your data about your use of the App services in order to provide you with personalised communications (by e-mail, telephone call, SMS message). Profiling shall only be carried out on the basis of your consent (consent to direct marketing) and personalised marketing shall be sent to you on the basis of your consent or on the basis of our legitimate interest to promote commerce and to provide you with offers that are relevant to you (where you are a customer of ours and you have not consented to receive direct marketing).

**Your rights**

You shall have the following rights:
•	To know about the processing of your Personal Data (right to know);
•	To get acquainted with your Personal Data and how it is processed (right of access);
•	To request rectification or completion of incomplete Personal Data (right to rectification);
•	To request the erasure of your Personal Data or the suspension of the processing of your Personal Data (with the exception of storage) (the right to suspend processing and the right to be "forgotten");
•	To request the Data Controller to restrict the processing of Personal Data on legitimate grounds (right of restriction);
•	To receive the Personal Data that you have provided to the Data Controller in a structured, commonly used and computer-readable format and to transfer that Personal Data to another Data Controller for a legitimate reason (right of portability);
•	To object to the processing of Personal Data where such Personal Data is processed on the basis of consent or legitimate interest (except where the Data Controller's interest in processing your Personal Data overrides your interest), including the right to object to the processing of Personal Data for the purpose of direct marketing, including profiling (right to object);
•	To withdraw your consent at any time to the processing of your Personal Data for the purposes set out in your consent, such as direct marketing, including profiling. Such withdrawal of consent shall not affect the lawfulness of the processing of Personal Data carried out prior to the withdrawal of consent (right to withdraw consent);
•	If you believe that we are violating your rights, you may file a complaint with the State Data Protection Inspectorate in accordance with the procedure established by law.

In order to exercise your rights as a data subject, you shall need to fill in a free-form request for the exercise of your data subject rights. You may (1) sign it in the usual way and present it at the Data Controller's office (the address shall be indicated in the Data Controller's details); or (2) you may sign the completed request with a qualified electronic signature and send it to the Data Protection Officer of Cgates by e-mail (the address shall be indicated in the Data Controller's details).

When you arrive at the office to submit your request, you will be asked to present the original signed application form and your personal identity document to verify your personal identity. This helps us to verify your personal identity and prevent unauthorised processing of your Personal Data. Only requests submitted in this way shall be considered submitted, evaluated and implemented. 

Upon receipt of such a request, we shall respond no later than one month from the date of the request. If necessary, the period may be extended by a further two months, depending on the complexity and number of requests. In such a case, we shall inform you of the extension within one month of receipt of the request, together with the reasons for the delay. The statutory term for the exercise of the data subject's rights shall begin to run from the date of a proper request made by one of the above-mentioned methods.

**Cookies**

We use cookies to make the App work and to personalise the ads we show you and to analyse the use of the App.

By accepting the use of cookies in the "Options" section of the App, you consent to us placing analytical, marketing and re-targeting cookies on your device. If you do not accept the use of cookies and continue to use the App, then only cookies that are necessary for the functioning of the App (technical and functional) and for which we are not required to obtain your consent shall be installed on your device. By accepting cookies, you also agree that third parties may install the specified cookies and use the information obtained through them.

A cookie is a medium of letters and numbers that we store on your device. We use essential (technical and functional) cookies to ensure the functioning of the App, which do not require your consent to install on your device.

The App uses the following types of cookies:
•	Technical cookies. Technical cookies are essential for the proper functioning of our website. Technical cookies help to display the content of our website on your device. Without them, it is not possible to make full use of our website. Technical cookies also ensure that if you give your consent, you do not have to do so again each time you visit our website.
•	Functional cookies. Functional cookies are designed to improve the functionality of the website and to make the website user-friendly and effective for your use.
•	Analytical cookies. Analytical cookies are used to compile a statistical analysis of the navigation patterns of visitors to our website; the data collected by these cookies is used without identifying you, i.e. we obtain personalised statistical information;
•	Marketing and re-targeting cookies. These are cookies that collect information about your visit to our website, which is then used to display repeated advertising on the internet (including various social networks (Facebook, etc.)), to carry out online marketing campaigns, and to measure the success of our marketing efforts. Most of these cookies are third-party cookies (see below) which we use for marketing/re-advertising purposes.
•	Session cookies are cookies that are used while a visitor is browsing the internet and are deleted automatically when the browser is closed;
•	Persistent cookies are stored in the visitor's browser until the cookie expires or until the visitor deletes them.
•	First-party cookies are our own cookies. 
•	Third party cookies: these cookies are provided by third parties in agreement with us.


**Use of cookies for analytics, marketing and re-targeting**

Google Analytics. We use Google Analytics, an App visitor usage analysis service provided by Google. Google Analytics is Google's analytics tool that makes it easier for website and application owners to understand how visitors use their website or application. We use a set of cookies to collect information and report on App usage statistics to Google without personally identifying individual visitors.

If you object to the recording of cookies on your device for analytical purposes, you may take advantage of Google's option to install an extension for the most commonly used web browsers that disables Google Analytics when you browse the internet. You can download the extension at https://tools.google.com/dlpage/gaoptout?hl=en. Installation of this extension will disable Google Analytics completely, not just our App.

Please also note that we use the additional Google Analytics Advertising features "Demographics and Interests" and "Re-targeting" in the App. For the operation of the "Demographics and Interests" feature, we use Google Analytics cookies to provide aggregated and anonymous demographic and interest information about App visitors. In the case of "Re-targeting", this feature allows the App to re-target visitors or those interested in our services via an advertising message with advertising messages that we administer on Google's partner websites. You shall have the option to disable personalised display of re-targeted advertising by changing your settings on https://adssettings.google.com.

For more information on how Google uses your information to advertise, visit https://safety.google/privacy/ads-and-data/?hl=lt.

Facebook Pixel. The App uses the re-targeting functionality provided by Meta Platforms Inc. ("Facebook"). This feature is used to optimise the advertising that is shown to App visitors when they use the Facebook social network or other websites with Facebook Pixel technology. To achieve this, the App has a Facebook re-advertising tag (FB pixel). When you use the App, the bookmark saves information about it. When you sign in to your Facebook account, information about your use of the App is transmitted to Facebook. The information collected by this function is anonymous, i.e. we cannot identify you on the basis of the information collected to support the operation of this feature. For more information about Facebook's re-targeting services, please refer to Facebook's privacy policy at https://www.facebook.com/about/privacy/. You also have the option to turn off re-targeted advertising by Facebook, which you can do by visiting https://www.facebook.com/settings (you must be logged in to your Facebook account in order to turn off personalised Facebook advertising).


**Restriction and refusal of the use of cookies**

Cookies are under your control and you can refuse or remove (delete, disable) them at any time by adjusting the settings of your App and your device/web browser. You can normally:
•	check what cookies are stored and delete individual cookies;
•	block third-party cookies;
•	block cookies from specific websites;
•	block the recording of all cookies;
•	delete all cookies when you close your browser.

If you do not consent to the placement of cookies on your device, you can withdraw your consent at any time by changing your settings and deleting the cookies that have been placed on your device, as well as by deactivating the provision of re-targeted advertising and the collection of analytics data by Facebook and/or Google in the ways mentioned above. If you choose to delete cookies, please remember that any options you have set will also be removed. As a reminder, many websites will not work properly if cookies are blocked completely.

You can find more information about cookies at: www.AllAboutCookies.org and http://www.youronlinechoices.eu.

For information on how to change your browser settings, see:
Internet Explorer / Firefox / Google Chrome / Safari / Opera


**Information about the cookies we use:**

Cookie	Source	Type	Purpose	Moment of creation / duration of storage

_app_lang	cgates.tv	Functional, first-party, persistent	To save the language selected in your App profile	After visiting the website / 1 year
connect.sid	cgates.tv	Functional, first-party, persistent	Stores login information for user authentication and secure login	After visiting the website / 1 hour
fusionauth.locale	cgates.tv	Functional, first-party, persistent	Intended to store individual choices.	After visiting the website / 13 months
fusionauth.remember-device	cgates.tv	Functional, first-party, persistent	Stores information that the user has chosen to remember App account login details on the device
	After visiting the website / 13 months
fussionauth.sso	cgates.tv	Functional, first-party, persistent	Stores information that the user has chosen not to remember to log in to the App account data on the device
	After visiting the website / 13 months
fussionauth.timezone	cgates.tv	Functional, first-party, session	Time zone stored on the device to adjust the time and date displayed in the App	After visiting the website / before the end of the session (while using the App)
profile	cgates.tv	Functional, first-party, persistent	Stores the options set in the App profile	After visiting the website / 1 hour
_ga_R6G6PKD419	cgates.tv	Analytical, first-party, persistent	Capturing the number of page views	After visiting the website / 2 years
_ga	cgates.tv	Analytical, first-party, persistent	Capturing the number of page views	After visiting the website / 2 years
_ga	cgates.lt	Analytical, first-party, persistent	Capturing the number of page views	After visiting the website / 2 years
_ga_WQSDLCXLOP	cgates.lt	Analytical, first-party, persistent	Capturing the number of page views	After visiting the website / 2 years
_fbp	Meta Platforms Inc.	Marketing and re-advertising, third party, persistent	Intended to capture and advertise on websites other than the App	After visiting the website / 3 months
_gid	cgates.tv	Analytical, first-party, persistent	Capturing the number of page views	After visiting the website / 24 hours
_gcl_au	cgates.lt	Marketing and re-targeting, first-party, persistent	Intended to record conversions	After visiting the website / 90 days

![](RackMultipart20210603-4-10h6pm3_html_f6274a618d2a53d.jpg)