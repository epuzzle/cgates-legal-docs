![](RackMultipart20210603-4-11y7wp1_html_674a7be6ce02648c.png)
**2024 m. kovo 1 d. redakcija**

**„Cgates TV" programėlės naudojimo sąlygos**

**I. Bendrosios sąlygos**

1.	„Cgates TV“ yra internetinė paslauga, teikiama per registruotus įrenginius (toliau – Paslauga). Įsigydami Paslaugą jūs (toliau – Klientas, Jūs, Jūsų, Jums, Jus) gausite prieigą ir galėsite žiūrėti televizijos kanalus, filmus, serialus tiesiogiai arba vėliau transliuojamą turinį vaizdo programų (įrašų) formatu srautinio duomenų siuntimo būdu (angl. streaming). Su naujausia informacija apie esamus paslaugų paketus galite susipažinti mūsų interneto svetainėje adresu www.cgates.lt arba per išmaniąją programėlę „Cgates TV“ (toliau kartu – Svetainė). 
2.	Paslaugą siūlo UAB „Cgates“, juridinio asmens kodas 120622256, registruotos buveinės adresas Ukmergės g. 120, Vilnius, tel. Nr. (8 5) 215 0000, el. pašto adresas info@cgates.lt. 
3.	Šios sąlygos (kartu su jose minimais dokumentais) taikomos ir yra teisiškai privalomos visiems Paslaugos vartotojams. Prieš naudodamiesi Paslauga Jūs turite perskaityti šias sąlygas ir su jomis sutikti. Naudodamiesi Paslauga Jūs sutinkate su šiomis sąlygomis ir įsipareigojate jų laikytis. Jei bet kuriuo metu Jūs nesutinkate su sąlygomis arba negalite jų laikytis, Jūs neturite teisės naudotis Paslauga. Bet kuriuo metu galite peržiūrėti sąlygas, kurios išsaugotos Svetainėje adresu https://www.cgates.lt/pagalba-klientams/duk/duk-sutartys/. Šios sąlygos kartu su Privatumo politika ir Slapukų naudojimo politika, taip pat Specialiosios sąlygos, Sąlygos dėl įrangos (jei tokios taikomos) yra neatsiejama sutarties dėl Paslaugos naudojimo (toliau – Sutartis), sudarytos tarp „Cgates“ ir Jūsų, dalis, pakeičianti bet kokius ankstesnius raštiškus ar žodinius susitarimus dėl šios Sutarties turinio.
4.	Sąlygos parengtos vadovaujantis Lietuvos Respublikos elektroninių ryšių įstatymu, Lietuvos Respublikos visuomenės informavimo įstatymu, Elektroninių ryšių paslaugų teikimo taisyklėmis ir kitais Lietuvos Respublikos norminiais teisės aktais.

**II. Sąvokos**

5.	Sąlygose vartojamos sąvokos:

5.1. Klientas - fizinis asmuo, kuris yra pasirašęs „Cgates" paslaugų teikimo sutartį ir naudojasi „Cgates" teikiamomis elektroninių ryšių paslaugomis asmeniniams, šeimos ar namų ūkio poreikiams, nesusijusiems su verslu ar profesija, tenkinti;

5.2. Paslauga - viena iš „Cgates" tinkle teikiamų paslaugų, dėl kurios/ių teikimo Klientas sudarė „Cgates" paslaugų teikimo sutartį;

5.3. Sutartis - Kliento ir „Cgates" sudaryta „Cgates" Paslaugų teikimo sutartis, Klientui pateikta Paslaugų teikimo sutarties santrauka, viešai skelbiamos taisyklės, šios Paslaugos naudojimo sąlygos bei Paslaugų tarifai, atskirai pateikiami Paslaugų užsakymai, kiti Sutarties priedai, pakeitimai bei papildymai, kurie po jų pasirašymo yra pateikti Kliento savitarnoje. Sutartis sudaroma Klientui pasirašius „Cgates" nustatytos formos „Cgates" Paslaugų teikimo sutartį ar jos priedą (užsakymą), arba kitais būdais (pavyzdžiui, žodžiu arba konkliudentiniais veiksmais) šalims susitarus dėl paslaugų teikimo. Šalių susitarimas Sutarties dalimi tampa nuo to momento, kai Klientui faktiškai pradedamos teikti minėta Sutartimi ar jos priedu (užsakymu) užsakytos Paslaugos;

5.4. „Cgates" tinklo galinis taškas - „Cgates" tinklo galinis įrenginys, prie kurio prijungta Klientui priklausanti ar „Cgates" Klientui perduota įranga.

5.5. „Cgates" Įranga - Klientui, sąlygose, Sutartyje ir/ar jos prieduose nustatyta tvarka, suteikiama įranga (įskaitant programinę įrangą), reikalinga „Cgates" teikiamoms Paslaugoms priimti;

5.6. Trumpiausias naudojimosi paslaugomis terminas - Kliento įsipareigojimas naudotis Paslaugomis ir/arba Įranga tam tikrą, Sutartyje ir/ar jos prieduose (užsakymuose) nurodytą, laikotarpį, kuris pradedamas skaičiuoti nuo Paslaugų teikimo pradžios, ir kuriam pasibaigus nustoja galioti Klientui suteiktos nuolaidos;

5.7. Pagrindinis TV programų rinkinys - TV programų rinkinys, kurį sudaro minimalus TV programų skaičius ir kuris teikiamas visiems Klientams už „Cgates" nustatytą minimalų mokestį. Tuo atveju, jei kyla neaiškumų, kuris iš „Cgates" platinamų TV programų rinkinių yra pagrindinis, pagrindiniu laikomas rinkinys, kurį sudaro mažiausias kiekis TV programų ir kuris teikiamas už mažiausią mokestį iš visų siūlomų rinkinių bei atitinka šias sąlygas: (i) jame privalomai retransliuojamos visos nekoduotos nacionalinės Lietuvos nacionalinio radijo ir televizijos transliuojamos televizijos programos ir kitos televizijos programos, kurioms Lietuvos radijo ir televizijos komisija suteikė privalomai retransliuojamos programos statusą; (ii) daugiau kaip 50 procentų į paketą įtrauktų televizijos programų yra transliuojamos oficialia Europos Sąjungos kalba. Pagrindiniai TV programų rinkiniai, teikiami skirtingomis technologijomis ir (arba) skirtingoms vartotojų kategorijoms, gali skirtis TV programų sudėtimi;

5.8. Papildomos paslaugos - tai Kliento papildomai užsakytos paslaugos (pavyzdžiui, papildomų TV programų rinkinių retransliavimo,  įrangos nuomos, programų įrašymo ir kt.), kurių teikimas tiesiogiai susijęs su Paslaugomis ir kurios negali būti teikiamos atskirai nuo Paslaugų;

5.9. Papildomi TV programų rinkiniai - TV programų rinkiniai, kuriuos galima užsakyti už papildomą mokestį tik kartu su pagrindiniu TV programų rinkiniu;

5.10. Paslaugų aktyvacija - tai „Cgates" tinkle/sistemose ir/ar Kliento valdoje/įrenginiuose atliekami darbai, skirti sudaryti reikiamas technines sąlygas, būtinas Paslaugoms teikti;

5.11. Paslaugų teikimo pradžia - laikas, nuo kurio Klientui sudaroma galimybė pradėti naudotis užsakytomis Paslaugomis sąlygose nustatyta tvarka;

5.12. „Cgates" tinklas - „Cgates" priklausantis ar valdomas elektroninių ryšių tinklas.

**III. Paslaugų užsakymas, aktyvacija ir teikimas**

6.	Paslaugas Klientas gali užsakyti Sutartyje, sąlygose arba „Cgates" viešai skelbiamais būdais. Nepriklausomai nuo užsakymo būdo, Klientas įsipareigoja apmokėti už „Cgates" Paslaugas ir kitus „Cgates" nustatytus mokesčius.
7.	Kliento užsakytos Paslaugos pradedamos teikti ne vėliau kaip per 10 darbo dienų nuo Paslaugų užsakymo dienos, jeigu Klientas įvykdo nustatytas sąlygas. Užsakyti ir valdyti „CGATES TV“ galima į savo įrenginį atsisiuntus mobilią/televizoriaus programėlę „CGATES TV“. „CGATES TV“ užsakymo metu Klientas turi būti ne jaunesnis (-ė) nei 18 metų. Kad veiktų „CGATES TV“, reikia turėti interneto prieigą (ne mažesnės kaip 1,5 Mbps spartos) bei įrenginius (televizorius, išmanusis telefonas/planšetė ir pan.), atitinkančius reikalavimus, nurodytus programėlės įsigijimo svetainėje.
8.	Gedimai „Cgates" Tinkle šalinami per 16 darbo valandų nuo pranešimo apie gedimą momento arba su Klientu suderintu laiku, jeigu gedimo nustatymui ir pašalinimui būtina vykti į paslaugos teikimo vietą.
9.	Gedimų šalinimo trukmė pradedama skaičiuoti nuo pranešimo apie gedimą momento. Gedimų, apie kuriuos buvo pranešta pasibaigus darbo dienai, savaitgaliais ir švenčių dienomis, šalinimo trukmė pradedama skaičiuoti nuo artimiausios darbo dienos 8.00 valandos. Į gedimų šalinimo trukmę neįskaičiuojami savaitgaliai, šventinės dienos ir gedimai apie kuriuos buvo pranešta pasibaigus darbo dienai.
10.	Gedimai šalinami darbo valandomis: pirmadieniais - penktadieniais 8.00 - 17.00 val., išskyrus atskirai sutartus atvejus.
11.	Papildomi TV programų rinkiniai gali būti užsakomi, tik užsakius pagrindinį TV programų rinkinį. Papildomos TV paslaugos gali būti užsakomos tik užsisakius pagrindinį TV programų rinkinį. Keičiantis rinkos sąlygoms ar taikomiems teisės aktams, taip pat esant kitiems objektyviems pagrindams (pvz., aplinkybėms, susijusioms su autorių teisių apsauga, priežiūros ir kitų institucijų sprendimais, sutartiniais įsipareigojimais, tiekėjų veiksmais ar neveikimu, kitomis komercinėmis priežastimis ir pan.), “Cgates” turi teisę vienašališkai be neigiamų pasėkmių pakeisti Pagrindinio ar Papildomų TV programų rinkinių turinį. “Cgates” apie tokius pakeitimus informuoja Klientą nedelsdama pranešimu savitarnoje ar kitokiu priimtinu būdu.
12.	Klientui sudarius nuotolinę ar ne prekybos patalpose sudaromą sutartį, Klientas turi teisę per keturiolika dienų atsisakyti Sutarties. Klientas sudarydamas nuotolinę sutartį patvirtina, jog pageidauja, kad paslaugos būtų pradėtos teikti nepasibaigus keturiolikos dienų Sutarties atsisakymo terminui, Klientas informuojamas ir sutinka, kad tokiu atveju nutraukęs Sutartį privalės apmokėti „Cgates“ pinigų sumą, lygią faktiškai suteiktų paslaugų kainai.

**IV. Šalių įsipareigojimai**

13.	„Cgates" įsipareigoja:

13.1.	Teikti Kliento užsakytas Paslaugas Sutartyje numatytomis sąlygomis ir tvarka iki „Cgates" priklausančio tinklo galinio taško.

13.2.	Paslaugų teikimo laikotarpiu konsultuoti Klientą atitinkamų paslaugų teikimo, techninės ir programinės įrangos, reikalingos paslaugos priėmimui bei komplektavimui klausimais. Su visa informacija apie „Cgates" paslaugas, jų teikimo tvarką, sąlygas, įkainius, Sutarties šalių atsakomybę ir kita su paslaugų teikimu susijusia informacija Klientas gali susipažinti interneto svetainėje www.cgates.lt, arba paskambinęs Sutartyje nurodytais telefono numeriais.

13.3.	Teikiant Kliento užsakytas Paslaugas, laikytis Paslaugų teikimo Sutarties, šiose sąlygose, Sutarties prieduose (užsakymuose) nustatytų reikalavimų bei Lietuvos Respublikos teisės norminių aktų, reglamentuojančių elektroninių ryšių veiklą, reikalavimų.


14.	Klientas įsipareigoja:

14.1.	Laiku ir tinkamai atsiskaityti už suteiktas Paslaugas nustatyta tvarka. Tai yra esminė Sutarties sąlyga. 

14.2.	Be „Cgates" sutikimo neperleisti Paslaugų ir/ar „Cgates" įrangos tretiesiems asmenims.

14.3.	Naudotis Paslaugomis Sutartyje ir šiose sąlygose nustatyta tvarka ir numatytiems tikslams. Nenaudoti Paslaugų neteisėtais tikslais, pažeidžiant kitų „Cgates" klientų teises, Lietuvos Respublikos įstatymus bei trečiųjų asmenų teises, nekenkti „Cgates“ tinklo vientisumui ir veikimui, agresyviai nesielgti „Cgates darbuotojų ar įgaliotų asmenų atžvilgiu, vykdyti „Cgates" teisėtus reikalavimus, būtinus tinkamam Paslaugų teikimui užtikrinti. Tai yra esminė Sutarties sąlyga.

14.4.	Paslauga prieinama Lietuvos teritorijoje, taip pat ES ir EEE valstybėje narėje, kai lankotės joje laikinai. „Cgates“ neatsako už galimas išlaidas, įstatymų pažeidimus ar bet kuriuo kitu atveju, įskaitant, pavyzdžiui, Jums kilusią atsakomybę, jei naudojatės ar mėginate naudotis Paslauga už Lietuvos ir ES bei EEE valstybių narių ribų.

14.5.	Pranešti apie Paslaugų sutrikimus ir savavališkai jų netaisyti. Klientas nemokamai gali užregistruoti gedimus ir/ar kreiptis dėl pagalbos (informacijos) apie Paslaugas paskambinęs Sutartyje nurodytais telefono numeriais. 

14.6.	Naudotis Paslaugomis terminu, kuris ne trumpesnis už trumpiausią naudojimosi Paslauga terminą, jeigu toks buvo nustatytas. Minimalus užsakomas papildomų TV programų rinkinio terminas - 1 mėnuo. Minimalus užsakomas Paslaugų terminas, pasirinkus išankstinį apmokėjimo būdą – 1 mėnuo. Klientui užsisakius Papildomų paslaugų, Trumpiausias naudojimosi paslaugomis terminas nepratęsiamas, nebent Klientas, užsisakydamas Papildomų paslaugų, aiškiai patvirtina savo sutikimą su Trumpiausio naudojimosi paslaugomis termino pratęsimu.

14.7.	Apie savo duomenų, nurodytų Sutartyje, pasikeitimą informuoti „Cgates" raštu arba telefonu nedelsiant, bet ne vėliau kaip per 5 (penkias) kalendorines dienas nuo pakeitimų atsiradimo dienos.

14.8.	Nenaudoti Paslaugos Lietuvos Respublikos įstatymais draudžiamai veiklai, kompiuterių virusams, masinei piktybiškai informacijai (spam) siųsti ar kitiems tikslams, kurie gali pažeisti trečiųjų asmenų teises ir teisėtus interesus.

14.9.	Jei Klientas įsiskolina „Cgates", jis apmoka visas išlaidas, susijusias su skolos išieškojimu, įskaitant, bet neapsiribojant įsiskolinimo administravimo mokesčiu bei kitomis su skolos išieškojimu „Cgates" patirtomis išlaidomis.

14.10.	Saugoti ir tretiesiems asmenims neperduoti Klientui suteikto slaptažodžio ar kitos informacijos skirtos Paslaugų užsakymui ir valdymui.

14.11.	Neplatinti, nekopijuoti, neatgaminti, viešai neskelbti, neretransliuoti, netransliuoti, neperduoti tretiesiems asmenims ar kitokiais būdais nepadaryti viešai prieinamu (įskaitant internetą) „Cgates" retransliuojamų programų ir bet kokių kitų rodomų kūrinių.

**V. Šalių teisės**

15.	„Cgates" turi teisę:

15.1.	Pasikeitus Lietuvos Respublikos teisės aktams, rinkos sąlygoms ar kitoms reikšmingoms ekonominėms sąlygoms, o taip pat esant kitiems objektyviems pagrindams, pakeisti Sutarties sąlygas, prieš 1 mėnesį raštu (pranešimu savitarnoje) ar kitomis ryšio priemonėmis informavusi apie tai Klientą. Klientui nesutikus su Sutarties sąlygų pakeitimu, Klientas turi teisę vienašališkai atsisakyti Paslaugų ir nutraukti Sutartį nepasibaigus trumpiausiam naudojimosi paslaugomis terminui be papildomų išlaidų, išskyrus atvejus, kai siūlomi pakeitimai yra Kliento naudai, yra tik administracinio pobūdžio ir nedaro neigiamo poveikio Klientui arba yra atliekami vykdant Europos Sąjungos ar nacionalinėje teisėje tiesiogiai nustatytus reikalavimus. Tokiu atveju Klientas privalo įspėti „Cgates" raštu arba el. paštu iki numatyto pakeitimo datos. Tokiu atveju Klientui netaikomi galiojantys įsipareigojimai dėl trumpiausio naudojimosi paslaugomis termino. Nepaisant aukščiau paminėto, „Cgates" bet kuriuo Sutarties galiojimo momentu, turi teisę pasiūlyti Klientui pakeisti ar papildyti sutartį.

15.2.	Jei Klientas nesilaiko Sutartyje ar jos prieduose numatytos mokėjimo tvarkos ar atlieka kitus esminių Sutarties ar sąlygų pažeidimus, „Cgates" turi teisę, įspėjusi trumpąja žinute Kliento nurodytu telefono numeriu ar elektroninio pašto adresu ne vėliau nei prieš 5 dienas, apriboti, sustabdyti arba nutraukti paslaugų teikimą. Klientui apmokėjus įsiskolinimą, kitą darbo dieną po informacijos apie visišką atsiskaitymą su „Cgates" gavimo, „Cgates“ įsipareigoja atnaujinti Paslaugos teikimą Klientui. Atnaujinus Paslaugų teikimą „Cgates" turi teisę pareikalauti iš Kliento apmokėti „Cgates" nustatytą paslaugų teikimo atnaujinimo mokestį. Informacijos apie visišką atsiskaitymą su „Cgates" gavimo diena laikoma diena, kai Klientas pateikia „Cgates" apmokėjimą įrodantį dokumentą arba Kliento mokėtinos sumos yra įskaitomos į „Cgates" sąskaitą. Klientui nepašalinus Sutarties pažeidimų per 30 dienų, „Cgates" turi teisę nutraukti Sutartį. 

15.3.	Pareikalauti iš Kliento išankstinio vieno mėnesio dydžio mokesčio už Paslaugas, jei Klientas yra įsiskolinęs „Cgates" už suteiktas Paslaugas.

15.4.	Jei Klientas savavališkai bando pasikeisti arba prisijungti naują teikiamą paslaugą (bandymas įsilaužti į „Cgates" sistemą, pakeisti jos konfigūraciją, melagingi prisistatymai ir t.t.), o taip pat iš Kliento kompiuterio/ių ar kitos Klientui priklausančios įrangos yra skleidžiamas virusų srautas, kuris neleidžia „Cgates" suteikti kokybišką interneto ryšį Klientui/ams, arba Klientas vėluoja atsiskaityti už paslaugas, arba Klientas savavališkai perdavė Paslaugas tretiems asmenims, ar kai yra akivaizdi saugumo incidento grėsmė arba nustatomas įvykęs saugumo incidentas, „Cgates" turi teisę laikinai sustabdyti/apriboti Paslaugos teikimą ir reikalauti iš Kliento kompensacijos už padarytą žalą. Bet kuriuo iš šiame punkte nurodytu pagrindu „Cgates" sustabdžius/apribojus paslaugų teikimą, Klientas neatleidžiamas nuo pareigos mokėti paslaugos/ų mokestį/čius.

15.5.	„Cgates" turi teisę kaupti bei teikti informaciją apie neatsiskaitantį Klientą tretiesiems asmenims, įstatymų nustatyta tvarka turintiems leidimą tvarkyti asmens duomenis, dėl skolos išieškojimo. Taip pat pareikalauti iš Kliento atlyginti visas pagrįstas su skolos už suteiktas Paslaugas išieškojimu susijusias išlaidas.

15.6.	Laikinai sustabdyti paslaugų teikimą Klientui apie tai jį tinkamai informavus elektroniniu paštu ir/ar telefonu ir/ar SMS žinute bei interneto svetainėje www.cgates.lt dėl atliekamų remonto, montavimo ir profilaktikos darbų.

15.7.	„Cgates", užtikrindama Paslaugų saugumą ir/ar tinkamą veikimą, turi teisę atlikti „Cgates" programinės įrangos atnaujinimus ir/ar būtinus nustatymų keitimus. Tokiais atvejais „Cgates" neatsako už įrangoje Kliento atliktų nustatymų pašalinimą ar pakeitimą, bei su tuo susijusią riziką ar žalą, kurią Klientas galėtų patirti.

15.8.	Apriboti paslaugų teikimą tais atvejais, kai „Cgates" negauna ar gauna neteisingą/nepilną informaciją apie Kliento mokėjimą dėl Kliento ar banko ar kitų mokesčius administruojančių įmonių kaltės. Tokiais atvejais mokėjimai už Paslaugas gali būti vertinami kaip nesumokėti iki tinkamos informacijos gavimo dienos. Toks Paslaugų teikimo ribojimas atnaujinamas per 1 darbo dieną nuo teisingos /pilnos informacijos apie mokėjimus gavimo dienos.

15.9.	Susisiekti su Klientu sutartyje nurodytais kontaktais ir informuoti Klientą apie Paslaugų teikimo sąlygų pasikeitimus.


16.	Klientas turi teisę:

16.1.	Atsisakyti „Cgates" Paslaugų, nutraukiant Sutartį šiose sąlygose nustatyta tvarka.

16.2.	Sumokėjęs „Cgates" nustatytus mokesčius ir laikydamasis „Cgates" nustatytos tvarkos, laikinai sustabdyti „Cgates" Sutartį. Minimali Sutarties  laikino sustabdymo trukmė 1 mėn., maksimali Sutarties laikino sustabdymo bendra trukmė 6 mėnesiai. Laikinas sustabdymas taikomas ne dažniau kaip vieną kartą per kalendorinius metus. Tokiu atveju Trumpiausias naudojimosi paslaugomis terminas prasitęsia tiek mėnesių, kiek buvo laikinas Sutarties sustabdymas. Pasibaigus laikino sustabdymo terminui, Paslaugos/ų teikimas atnaujinamas automatiškai.

16.3.	Sutartyje ir sąlygose nustatyta tvarka gauti kompensaciją už Paslaugos teikimo sutrikimus, kilusius dėl „Cgates" kaltės.

16.4.	Savo nuožiūra pasirinkti „Cgates" siūlomus apmokėjimo už Paslaugas būdus bei tvarką. Už teikiamas paslaugas sumokėti iš anksto.

16.5.	Užsakęs „Cgates“ išmaniosios televizijos paslaugą, Klientas turi teisę, „Cgates" nustatytomis sąlygomis, tvarka ir Cgates nustatytam terminui (visos sąlygos pateikiamos www.cgates.lt), savo asmeniniam naudojimui užsakyti programų įrašymo ir kitas interaktyvias paslaugas. Klientui užsakius, Kliento asmeniniam naudojimui, „Cgates" atliks TV transliacijų įrašymą ir atgaminimą. Išimtinai Klientas atsako už savo duomenų, patalpintų į „Cgates" saugyklą, teisėtumą. Klientas privalo apmokėti už savo užsakytas papildomas mokamas paslaugas. 

16.6.	Jeigu Klientui įranga, kurią įrangos pirkimo – pardavimo sutarties pagrindu jam perleidžia „Cgates", netinka, Klientas turi teisę grąžinti įrangą per 14 dienų nuo jos įsigijimo dienos. Teisė atsisakyti įrangos pirkimo-pardavimo sutarties įgyvendinama įrangos pirkimo-pardavimo sutartyje numatytomis sąlygomis ir tvarka. Įranga grąžinama per „Cgates“ atstovą įrangos pirkimo-pardavio sutartyje numatytomis sąlygomis ir tvarka. 

**VI. Atsiskaitymo už paslaugas tvarka**

17.	Klientas už Paslaugas atsiskaito pagal Sutartyje ar jos prieduose nustatytus Paslaugų tarifus nustatyta tvarka. Ataskaitinis laikotarpis - vienas kalendorinis mėnuo. Paslaugų kredito limitas nenustatomas, išskyrus atvejus kai Šalys susitaria kitaip. Papildomi mokesčiai taikomi: (i) detali mokėjimų išklotinė (nuo 7 mėn. iki 24 mėn. imtinai) 5.00 Eur; (ii) detali mokėjimų išklotinė (virš 24 mėn.) 20.00 Eur.
18.	Apmokėjimui už „CGATES TV“ paslaugą gali būti taikomi šie būdai: išankstinis apmokėjimas arba apmokėjimas po paslaugos suteikimo. Priklausomai nuo „CGATES TV“ paslaugos užsakymo metu naudojamo atsiskaitymo būdo, sąskaitą klientui pateikiama už praėjusį kalendorinį mėnesį arba sąskaita pateikiama išankstiniam apmokėjimui elektroniniu būdu siunčiant sąskaitą (arba nuorodą į sąskaitą) Kliento nurodytu el. pašto adresu arba pateikiama savitarnos svetainėje iki einamojo mėnesio vidurio arba prieš užsakant „CGATES TV“ paslaugą, kuri apmokama iki einamojo mėnesio paskutinės dienos arba prieš užsakant „CGATES TV“ paslaugą, o jeigu sąskaitoje nurodyta vėlesnė diena, – iki sąskaitoje nurodytos dienos. Apmokėjus pateiktą sąskaitą, Klientas patvirtina užsakytas paslaugas. Sąskaitos negavimas ne dėl „Cgates“ kaltės neatleidžia Kliento nuo pareigos atsiskaityti už Paslaugas.
19.	Neapmokėjus mokesčio už paslaugas aukščiau paminėtais terminais už kiekvieną pavėluotą dieną gali būti skaičiuojami 0.02 % dydžio delspinigiai. Klientui vėluojant apmokėti už paslaugas taikomas įsiskolinimo administravimo mokestis.
20.	„Cgates" sąskaitą už teikiamas paslaugas nemokamai pateikia Klientui elektroniniu būdu, t.y. Kliento savitarnoje. Jeigu Klientas pageidauja „Cgates“ sąskaitą už teikiamas paslaugas gauti neregistruotu paštu, Klientas turi sumokėti popierinės sąskaitos siuntimo mokestį, nurodytą https://www.cgates.lt/pagalba-klientams/duk/duk-sutartys/. Sąskaitos negavimas ne dėl „Cgates" kaltės neatleidžia Kliento nuo pareigos tinkamai atsiskaityti už Paslaugas. Už suteiktas Paslaugas „Cgates" kas mėnesį iki 10 mėnesio dienos išrašo ir Klientui pateikia sąskaitas už praeitą ataskaitinį laikotarpį, kurios turi būti apmokėtos iki einamojo mėnesio paskutinės dienos. Jeigu sąskaitoje nurodyta vėlesnė diena - iki sąskaitoje nurodytos dienos. Pasikeitus PVM tarifui atitinkamai perskaičiuojami ir visi Sutartyje numatyti mokėjimai už Paslaugas. Visi šiame Sutarties punkte numatyti mokesčių pasikeitimai apskaičiuojami neatsižvelgiant į tai ar yra nustatytas Trumpiausias naudojimosi paslaugomis terminas, ar ne.
21.	Su pirma sąskaita Klientas sumoka „Cgates" nustatytą paslaugos aktyvacijos mokestį ir už faktiškai suteiktas Paslaugas. Neapmokėjus mokesčio už paslaugas aukščiau paminėtais terminais už kiekvieną pavėluotą dieną gali būti skaičiuojami 0.02 % dydžio delspinigiai. Nutraukus sutartį, „Cgates" negrąžina aktyvacijos mokesčio bei sumokėtų mokesčių už suteiktas Paslaugas.
22.	Paslaugų teikimo pradžios diena yra įskaitoma į atsiskaitomąjį laikotarpį, už kurį turi būti mokama. Šiame punkte nustatyta tvarka taikoma ir nutraukiant Paslaugas ar laikinai sustabdant Sutartį.
23.	Jei iš Kliento gauto apmokėjimo neužtenka visam įsiskolinimui už Paslaugas padengti, gautas apmokėjimas paskirstomas taip: pirmiausiai padengiamos „Cgates" patirtos išlaidos išieškant skolą iš Kliento, toliau padengiami priskaičiuoti delspinigiai ir/ar palūkanos, paskiausiai padengiamas seniausiai susidaręs įsiskolinimas. Tuo atveju, jeigu dėl techninių galimybių Kliento įmokos buvo panaudotos ginčijamam įsiskolinimui padengti, „Cgates“ kompensuoja Klientui pritaikytus papildomus mokesčius už vėlavimą atsiskaityti bei ginčytą įsiskolinimą ar jo dalį, jeigu ginčas išsprendžiamas Kliento naudai visiškai arba iš dalies.
24.	Klientas, nutraukęs Sutartį ir/ar atsisakęs Paslaugos privalo atsiskaityti su „Cgates" už suteiktas Paslaugas iki Paslaugų atsisakymo dienos. Jeigu nėra pasibaigęs Trumpiausias naudojimosi Paslauga terminas - Klientas turi sumokėti visus mokesčius, susijusius su faktiškai suteiktomis Paslaugomis, taip pat Paslaugų aktyvacijos ar kitus „Cgates" nustatytus mokesčius (jei tokie nebuvo sumokėti), sumokėti suteiktas nuolaidas ir atlyginti kitas „Cgates" išlaidas, kurias patyrė „Cgates" norėdama įvykdyti Sutartį iki Sutarties nutraukimo dienos. Jeigu Sutartis nutraukiama dėl „Cgates" kaltės, Klientas privalo sumokėti visus mokesčius, susijusius su faktiškai ir tinkamai suteiktomis Paslaugomis, taip pat Paslaugų aktyvacijos mokesčius.

**VII. Paslaugų atsisakymo ir nutraukimo sąlygos**

25.	Klientui sudarius terminuotą paslaugų teikimo sutartį su „Cgates“, kurioje numatytas Trumpiausias naudojimosi paslaugomis laikotarpis, Klientas gali atsisakyti Paslaugų iš anksto, tačiau ne ankščiau nei prieš 3 mėnesius ir ne vėliau kaip prieš 5 (penkias) darbo dienas raštu apie tai pranešęs „Cgates". Klientui sudarius išankstinio apmokėjimo neterminuotą sutartį, Klientas gali bet kada atsisakyti „CGATES TV“ paslaugos. Tai galima atlikti prisijungus prie savo vartotojo „Cgates“ paskyros. Visais atvejais Klientui atsisakant/nutraukiant sutartį, taikomos šių sąlygų 23 punkto nuostatos.
26.	„Cgates" gali vienašališkai, nesikreipdama į teismą, nutraukti Sutartį ar vieną iš Kliento užsakytų paslaugų iš anksto, ne vėliau kaip prieš 5 (penkias) darbo dienas apie tai raštu pranešusi Klientui, jeigu Klientas pažeidžia esmines Sutarties, šių sąlygų ar kitų Sutarties priedų sąlygas bei dėl kitų objektyvių priežasčių. Esminėmis Sutarties sąlygomis visais atvejais bus laikomos sąlygos nurodytos šių sąlygų 14.1 – 14.11  punktuose. 

**VIII. Šalių atsakomybė**

27.	„Cgates" neatsako už atvejus, kai Klientas negalėjo naudotis Paslaugomis ir/ar Klientui padarytą žalą, jeigu tai įvyko ne dėl „Cgates" kaltės.
28.	Naudodamasis Paslaugomis, Klientas pilnai atsako už savo ir kitų asmenų, kurie naudojasi Klientui teikiamomis Paslaugomis, veiksmus bei padarytą žalą „Cgates" ar tretiesiems asmenims.
29.	Klientas pilnai atsako už bet kokios informacijos, kurią jis teikia ar gauna, naudodamasis „Cgates" Paslaugomis, turinį, įskaitant, bet neapsiribojant teisės aktų reglamentuojančių intelektinę nuosavybę, asmens duomenų apsaugą, reklamos paslaugų teikimą ir kitų, pažeidimą.
30.	Jeigu Klientas pažeidžia šių „CGATES TV“ paslaugos naudojimosi sąlygų nuostatas, „Cgates“ turi teisę visiškai ar iš dalies apriboti Klientui naudojimąsi „CGATES TV“ paslaugomis. „Cgates“ turi teisę nedelsiant ir be išankstinio įspėjimo apriboti ar sustabdyti paslaugų teikimą, jeigu Klientas pažeidžia šias esmines naudojimosi paslaugomis sąlygas:

30.1. naudojantis „CGATES TV“ pažeidžia trečiųjų asmenų teises ir teisėtus interesus bei viešosios tvarkos principus;

30.2. be „Cgates“ leidimo Klientas įrašo, kopijuoja ar kitokiu būdu atgamina TV programų ar kitą paslaugomis gaunamą turinį;

30.3. be „Cgates“ leidimo Klientas viešai skelbia (transliuoja, retransliuoja, padaro viešai prieinamu internetu), viešai platina ir bet kokiu kitu būdu, nei nustatyta šiose sąlygose, naudoja „CGATES TV“ programas ar jų dalis bei kitą paslaugomis gaunamą turinį;

30.4. Klientui draudžiama atlikti bet kokius veiksmus, kuriais bandoma įsilaužti į „CGATES TV“ programėlę, o pažeidus šį draudimą, visi asmenys yra baudžiami įstatymų numatyta tvarka. Pažeidimo atveju „Cgates“ turi teisę nedelsdama ir iš anksto neįspėjusi pažeidėjo kreiptis į teismą dėl neteisėta veika padarytos žalos atlyginimo;

30.5. Klientas įsipareigoja neatskleisti prisijungimo prie „CGATES TV“ paskyros duomenų tretiesiems asmenims. Siekiant saugumo, tretiesiems asmenims naudotis Kliento paskyra draudžiama. Pažeidimo, padaryto naudojantis konkrečia paskyra, atveju už pažeidimą ir dėl jo atsiradusios žalos atlyginimą atsako paskyrą sukūręs asmuo.

30.6. jeigu sudaroma Klientui galimybė naudotis „CGATES TV“ paslauga ES šalyse, toks naudojimasis turi būti laikino pobūdžio. Esant piktnaudžiavimui, „Cgates“ gali reikalauti pateikti papildomą informaciją ir negavus tokios informacijos apriboti arba sustabdyti paslaugas.

31.	Klientas įsipareigoja atlyginti „Cgates“, autorių bei gretutinių teisių turėtojams, taip pat visiems kitiems tretiesiems asmenims žalą, atsiradusią dėl Kliento veiksmų.
32.	Šalys atleidžiamos nuo atsakomybės už įsipareigojimų pagal Sutartį nevykdymą ar netinkamą vykdymą dėl neįveikiamų aplinkybių (force majeure) Lietuvos Respublikos teisės aktų nustatyta tvarka.
33.	Šalis privalo visiškai atlyginti kitos šalies tiesioginius nuostolius, jeigu žala atsirado dėl šalies kaltų veiksmų.
34.	Nė viena iš šalių neatsako už netiesioginių nuostolių ar neturtinės žalos atlyginimą kitai Sutarties šaliai.

**IX. Duomenų apsauga**

35.	„Cgates" svarbu ne tik paslaugų kokybė bei saugumas, bet ir Klientų duomenų apsauga. Pasirašydamas Cgates sutartį, Klientas patvirtina, kad yra supažindintas su teise žinoti (būti informuotas) apie savo asmens duomenų tvarkymą, susipažinti su savo asmens duomenimis ir kaip jie tvarkomi, reikalauti ištaisyti ar sunaikinti savo asmens duomenis šiose sąlygose, asmens duomenų teisinės apsaugos įstatymo bei kitų teisės aktų nustatytais atvejais ir tvarka, nesutikti dėl savo asmens duomenų tvarkymo. Klientas pateikęs asmens tapatybę patvirtinantį dokumentą turi teisę kreiptis, kad „Cgates" ištaisytų neteisingus, neišsamius ar netikslius duomenis, bei nesutikti ir/ar atšaukti teisę, kad asmens duomenys būtų tvarkomi tiesioginės rinkodaros tikslu.
36.	„Cgates" Klientų duomenis tvarko vadovaudamasi 2016 m. balandžio 27 d. Europos Parlamento ir Tarybos reglamento (ES) 2016/679 dėl fizinių asmenų apsaugos tvarkant asmens duomenis ir dėl laisvo tokių duomenų judėjimo ir kuriuo panaikinama Direktyva 95/46/EB (Bendrasis duomenų apsaugos reglamentas) (toliau - BDAR) 6 straipsnio 1 dalies a, b ir f punktais bei LR Asmens duomenų teisinės apsaugos įstatymo ir LR Elektroninių ryšių įstatymo bei kitų teisės aktų, reglamentuojančių duomenų apsaugą, nuostatas.
37.	Detaliau su asmens duomenų apsauga susijusios nuostatos yra aprašytos CGATES TV privatumo politikoje, kuri skelbiama viešai „Cgates" interneto puslapyje www.cgates.lt , „Cgates" klientų savitarnoje ir/arba programėlėje.  
38.	Įgyvendinant BDAR nuostatas „Cgates" yra paskirtas duomenų apsaugos pareigūnas (DAP). DAP el. paštas duomenu.apsauga@cgates.lt.
39.	Sutarties galiojimo metu „Cgates“ turi teisę teikti Klientui informaciją apie „Cgates“ paslaugas, jų funkcijas, atnaujinimus, taip pat nuolaidas ar išskirtinius pasiūlymus bei kitą su paslaugomis susijusią informaciją, šiam tikslui panaudojant Kliento asmens duomenis, įskaitant (bet neapsiribojant) vardą, pavardę, telefono numerį, elektroninio pašto adresą. Kliento asmens kodas šiuo tikslu nėra tvarkomas. Atsisakyti aukščiau aprašytų pranešimų galima bet kuriuo metu kreipiantis į „Cgates“ telefonu (8 5) 215 0000 ar el. paštu duomenu.apsauga@cgates.lt. Bet kuriuo atveju, Sutarties galiojimo metu „Cgates“ turi teisę telefonu, SMS žinute, elektroniniu paštu ar savitarnos pranešimu kreiptis į Klientą dėl Sutarties vykdymo.
40.	Klientas turi teisę pateikti skundą dėl jo asmens duomenų tvarkymo priežiūros institucijai: Valstybinė duomenų apsaugos inspekcija, L. Sapiegos  g. 17 Vilnius, tel. (8 5) 271 2804, 279 1445, el. paštas ada@ada.lt.

**X. Baigiamosios nuostatos**
41.	Šios sąlygos galioja nuo 2024 m. kovo 1 d. Sąlygos yra viešai prieinamas dokumentas, skelbiamas viešai „Cgates" interneto puslapyje www.cgates.lt , „Cgates" klientų savitarnoje ir/arba programėlėje.  „Cgates" atkreipia Kliento dėmesį, kad sąlygas Klientas gali atsisiųsti į savo asmeninę laikmeną dokumentavimo, informacijos atgaminimo jos nepakeičiant ir peržiūros ateityje tikslais. „Cgates“ turi teisę keisti šias sąlygas apie tai informuodama vartotojus „Cgates“ klientų savitarnoje ar kitu priimtinu būdu ir pateikdama naują sąlygų redakciją „Cgates“ interneto puslapyje www.cgates.lt. 
42.	Visos nuosavybės teisės, įskaitant intelektines, susijusios su programomis bei bet kokia su jomis susijusi techninė dokumentacija, prekių ženklais ir kitais žymenimis, autorių turtinėmis teisėmis, kurią Klientui pateikia „Cgates“, lieka „Cgates“ arba jos licenciarų nuosavybe.
43.	Nė viena šalis negali perduoti trečiajai šaliai savo teisių ar įsipareigojimų, atsiradusių vykdant sutartį, be kitos šalies išankstinio rašytinio sutikimo, išskyrus teisių ir įsipareigojimų perleidimą teisėtam teisių perėmėjui pagal teisės aktus. Šios nuostatos pažeidimu nelaikomi atvejai, kai „Cgates“, vykdydama sutartį, paveda atlikti tam tikras funkcijas ar darbus tretiesiems asmenims, likdama visiškai atsakinga už trečiuosius asmenis Klientui.
44.	Ginčai dėl sutarties nevykdymo ar netinkamo vykdymo sprendžiami šalių susitarimu, o šalims nesusitarus - Lietuvos Respublikos įstatymais nustatyta tvarka. Klientas informuojamas, kad gali kreiptis į ginčus tarp vartotojų ir elektroninių ryšių paslaugų teikėjų neteismine tvarka sprendžiantį subjektą - Lietuvos Respublikos Ryšių reguliavimo tarnybą (Mortos g. 14, Vilnius, tel. (8 5) 210 5633, faks. (8 5) 216 1564, el. paštas rrt@rrt.lt, interneto svetainė www.rrt.lt), ar užpildyti prašymo formą Elektroninėje vartotojų ginčų sprendimo platformoje adresu https://ec.europa.eu/odr/. Informacija apie ginčų dėl Sutarties nevykdymo arba netinkamo vykdymo sprendimo tvarką taip pat skelbiama viešai „Cgates" interneto puslapyje www.cgates.lt. 
45.	Jei kuri nors iš šių sąlygų Lietuvos Respublikos įstatymų nustatyta tvarka būtų pripažinta negaliojančia ar netaikytina, kitos šių sąlygų nuostatos toliau galios ir bus taikomos.

Paslaugos „CGATES TV“ tiekėjas:
UAB Cgates, Ukmergės g. 120, LT-08105 Vilnius, Lietuva
Kodas 120622256
Klientų aptarnavimas telefonu +370 5 215 0000
Interneto svetainė [www.cgates.lt](http://www.cgates.lt/), el. paštas info@cgates.lt 

Pageidauju gauti pranešimus apie paslaugas, pasiūlymus ir teikiamas nuolaidas:


<input type="checkbox" id="consent_sms_email" name="consent_sms_email" /> <label for="consent_sms_email"> SMS </label>

<input type="checkbox" id="consent_call" name="consent_call" /> <label for="consent_call"> Skambučiu </label>
